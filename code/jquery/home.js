function deleteStatus(){
    $(document).find('.status_container').find('.delete').on('click', function(e) {
        e.preventDefault();
        var status_id = $(this).parents('.status').attr('id');
        $.ajax({
           type: 'POST',
           url: 'status/obrisistatus',
           data: {'status_id': status_id},
           success: function(html) {
               var id='#' + status_id;
               $(id).remove();
           }
        });
    });
}

function setNewStatusProfileFunction(status_id){
    var id = '#' + status_id;
    $(id).find('.statusimglink').on('click', function(e) {
        e.preventDefault();
        $(document).find('.navbar').find('.active').removeClass();
        var korisnik_id = $(this).attr('id');
        $.ajax({
           type: 'POST',
           url: 'profil/profilishkec',
           data: {'korisnik_id': korisnik_id},
           success: function(html) {
               $(document).find('.row:first').remove();
               $(document).find('.row:first').html(html);
           }
        });
    });
}

function setStatusProfileFunction(statusi){
    statusi.find('.statusimglink').on('click', function(e) {
        e.preventDefault();
        $(document).find('.navbar').find('.active').removeClass();
        var korisnik_id = $(this).attr('id');
        $.ajax({
            type: 'POST',
            data: {'korisnik_id': korisnik_id},
            url: 'profil/profilishkec',
            success: function(html) {
                $(document).find('.row:first').remove();
                $(document).find('.row:first').html(html);
            }
        });
    });

}

function toggleActivePredmet(elem) {
    if($(elem).attr('class') === 'notactive') {
        $('#predmeti_odabir').children().each(function() {
            if($(this).attr('class') != 'divider'){
                $(this).prop('class', 'notactive');
            }
        });
        $(elem).prop('class', 'active');
    } else {
        $(elem).prop('class', 'notactive');
    }
}

function setLajkFunctions(statusi) {
    statusi.find('.lajk').on('click', function(e) {
        e.preventDefault();
        var dugme = this;
        var status_id = $(this).parents('.status').attr('id');
        $.ajax({
            type: 'POST',
            data: {'status_id': status_id},
            url: 'status/lajk',
            success: function(br_lajkova) {
                $(dugme).parents('.lajkdislajk').find('.br_lajkova').text(br_lajkova);
            }
        });
    });
}

function setDislajkFunctions(statusi) {
    statusi.find('.dislajk').on('click', function(e) {
        e.preventDefault();
        var dugme = this;
        var status_id = $(this).parents('.status').attr('id');
        $.ajax({
            type: 'POST',
            data: {'status_id': status_id},
            url: 'status/dislajk',
            success: function(br_dislajkova) {
                $(dugme).parents('.lajkdislajk').find('.br_dislajkova').text(br_dislajkova);
            }
        });
    });
}

function setNewStatusLajkFunction(status_id) {
    var id = '#' + status_id;
    $(id).find('.lajk').on('click', function(e) {
        e.preventDefault();
        var dugme = this;
        var status_id = $(this).parents('.status').attr('id');
        $.ajax({
            type: 'POST',
            data: {'status_id': status_id},
            url: 'status/lajk',
            success: function(br_lajkova) {
                $(dugme).parents('.lajkdislajk').find('.br_lajkova').text(br_lajkova);
            }
        });
    });
}

function setNewStatusDislajkFunction(status_id) {
    var id = '#' + status_id;
    $(id).find('.dislajk').on('click', function(e) {
        e.preventDefault();
        var dugme = this;
        var status_id = $(this).parents('.status').attr('id');
        $.ajax({
            type: 'POST',
            data: {'status_id': status_id},
            url: 'status/dislajk',
            success: function(br_dislajkova) {
                $(dugme).parents('.lajkdislajk').find('.br_dislajkova').text(br_dislajkova);
            }
        });
    });
}

function setKomentarFunctions(statusi) {
    statusi.find('#komentararea').on('keypress', function(e) {
        var code = e.keyCode || e.which;
        if(code === 13) {
            e.preventDefault();
            var textarea = this;
            var status = $(textarea).parents('.status');
            var status_id = $(status).attr('id');
            var container = $(status).next('.komentar_container');
            var telo = $(textarea).val();
            $.ajax({
                type: 'POST',
                data: {'telo': telo, 'status_id': status_id},
                url: 'komentar/add',
                success: function(komentar) {
                    $(komentar).hide().prependTo(container).slideDown('slow');
                    //$(container).prepend(komentar).fadeIn('slow');
                    $(textarea).val('');
                }
            });
        }
    });
}

function setNewStatusKomentarFunction(status_id) {
    var id = '#' + status_id;
    $(id).find('#komentararea').on('keypress', function(e) {
        var code = e.keyCode || e.which;
        if(code === 13) {
            e.preventDefault();
            var textarea = this;
            var status = $(textarea).parents('.status');
            var status_id = $(status).attr('id');
            var container = $(status).next('.komentar_container');
            var telo = $(textarea).val();
            $.ajax({
                type: 'POST',
                data: {'telo': telo, 'status_id': status_id},
                url: 'komentar/add',
                success: function(komentar) {
                    $(komentar).hide().prependTo(container).slideDown('slow');
                    $(textarea).val('');
                }
            });
        }
    });
}

function setDownloadFunction(status_id) {
    var id = '#' + status_id;
    $(id).find('#download_file').on('click', function(e) {
        e.preventDefault();
        var data = {'status_id': status_id};
        $.ajax({
            type: 'POST',
            url: 'status/download',
            data: data,
            success: function(url) {
                window.open(url, '_newtab');
            }
        });
    });
}

function setDownloadFunctions(statusi) {
    statusi.each(function() {
        var status_id = $(this).attr('id');
        setDownloadFunction(status_id);
    });
}

function postavi_error(error) {
    $('#validation_errors').empty();
    $('#validation_errors').prepend(error);
}

function postavi_najnoviji_status() {
    var status_id = $('.status_container').children().first().attr('id');
    var data = {'status_id': status_id};
    $.ajax({
        type: 'POST',
        url: 'proveravac/najnoviji',
        data: data,
        dataType: 'json',
    });
}

function postavi_form(action) {
    var telo = $('#status_textarea').val();
    var predmet_id = $('#predmeti_odabir').find('.active').attr('id');
    var data = {'telo': telo, 'predmet_id': predmet_id};
    
    var file = document.getElementById('userfile').files[0];
    if(file) {
        $.ajaxFileUpload({
            type: 'post',
            url: 'status/add',
            data: data,
            fileElementId: 'userfile',
            success: function(data, uspeh) {
                var status = data.activeElement.innerHTML;
                if($(status).first().attr('class') === 'error') {
                    postavi_error(status);
                    return;
                }
                //ubacuje status
                $(status).hide().prependTo('.status_container').slideDown('slow');
                //brise sve iz textarea za kucanje statusa
                $('#status_textarea').val('');
                
                $('#filename').text('');

                $('#predmeti_odabir').children().each(function() {
                    if($(this).attr('class') !== 'divider') {
                        if($(this).text() !== 'Info') {
                            $(this).prop('class', 'notactive');
                        } else {
                            $(this).prop('class', 'active');
                        }
                    }
                });

                var status_id = $(status).attr('id');
                //postavlja funkcije za lajk i dislajk
                setNewStatusLajkFunction(status_id);
                setNewStatusDislajkFunction(status_id);
                //postavlja funkcije za komentarisanje       
                setNewStatusKomentarFunction(status_id);
                //postavlja funkciju za download fajla
                setDownloadFunction(status_id);
                deleteStatus();
                
                postavi_najnoviji_status();
            }
        });
    } else {
        $.ajax({
            type: 'POST',
            data: data,
            url: action,
            cache: false,
            success: function(status) {
                if($(status).first().attr('class') === 'error') {
                    postavi_error(status);
                    return;
                }
                //ubacuje status
                $(status).hide().prependTo('.status_container').slideDown('slow');
                //brise sve iz textarea za kucanje statusa
                $('#status_textarea').val('');
                
                $('#filename').text('');

                $('#predmeti_odabir').children().each(function() {
                    if($(this).attr('class') !== 'divider') {
                        if($(this).text() !== 'Info') {
                            $(this).prop('class', 'notactive');
                        } else {
                            $(this).prop('class', 'active');
                        }
                    }
                });

                var status_id = $(status).attr('id');
                //postavlja funkcije za lajk i dislajk
                setNewStatusLajkFunction(status_id);
                setNewStatusDislajkFunction(status_id);
                //postavlja funkcije za komentarisanje       
                setNewStatusKomentarFunction(status_id);
                
                postavi_najnoviji_status();
                setNewStatusProfileFunction(status_id);
                deleteStatus();
            }
        });
    }
}

function loadall_komentar(statusi) {
    $(statusi).each(function(i) {
        var status_id = $(this).attr('id');
        var container = $(this).next('.komentar_container');
        $.ajax({
            type: 'POST',
            data: {'status_id': status_id},
            url: 'komentar/load_for_status',
            success: function(komentar) {
                $(komentar).hide().prependTo(container).slideDown('slow');
            }
        });
    });
}

function loadall_status(pocetni_id, to_empty, to_append) {
    var data = {'pocetni_status_id': pocetni_id};
    $.ajax({
        type: 'POST',
        url: 'status/loadall',
        data: data,
        success: function(status) {
            if(status == "") return;
            if(to_empty) {
                $('.status_container').empty();
            }
            if(to_append) {
                $('.status_container').find('.last').prop('class', 'thumbnail status');
                $(status).appendTo('.status_container');
            } else {
                $(status).prependTo('.status_container');
            }
            
            var statusi = $('.status_container').children('.status');
            //nakon ucitavanja statusa postavljanje funkcija
            //za lajk i dislajk
            setLajkFunctions(statusi);
            setDislajkFunctions(statusi);
            deleteStatus();
            
            //nakon ucitavanj statusa postavljanje funkcija
            //za dodavanje komentara
            setStatusProfileFunction(statusi);
            
            setKomentarFunctions(statusi);
            
            setDownloadFunctions(statusi);
            
            //ucitavamo i komentare ako postoje na statusima
            loadall_komentar(statusi);
            
            postavi_najnoviji_status();
            window.loading = 0;
            
        }
    });
}

function init_forms() {    
    $('form').submit(function(e) {
        e.preventDefault();
        var id = $(this).attr('id');
        var action = $(this).attr('action');
        
        //formama se daje id po kojem ih razlikujemo
        if(id === 'postavi_form') {
            postavi_form(action);
        }
    });
    
    $('#predmeti_odabir').children().on('click', function(e){
        e.preventDefault();
        toggleActivePredmet(this);
    });
    
    $('#userfile').hide();
    
    $('#userfile').on('change', function() {
        var name = $(this)[0].files[0].name;
        $('#filename').text(name);
    });
    
    $('#upload_file').on('click', function(e) {
        e.preventDefault();
        $('#userfile').click();
    });
    
    $(document).on('dragenter', function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    
    $(document).on('dragover', function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    
    $('#status_textarea').on('drop', function(e){
        if(e.originalEvent.dataTransfer) {
            if(e.originalEvent.dataTransfer.files.length) {
                e.preventDefault();
                e.stopPropagation();
                var files = e.originalEvent.dataTransfer.files;
              
                $('#userfile').prop('files', files);
            }
        }
    });
}

$(document).ready(function() {
    //ucitavanje statusa
    loadall_status(0, true, false);
    
    //ajax za submit formi
    init_forms();
        
    window.loading = 0;
    $(window).on('scroll', function(e) {
        if(window.loading === 0) { 
            if($(window).scrollTop() + $(window).height() > $(document).height() * 9.0/10.0) {
                window.loading = 1;
                var pocetni_id = $('.status_container').find('.last').attr('id');
                loadall_status(pocetni_id, false, true);
            }
        }
    });
         
});