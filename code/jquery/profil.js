$(document).ready(function() {

    $(document).on('click','#edit', function(ev) {
        ev.preventDefault();
        $('#tabovi a[href="#o_korisniku"]').tab('show');
        
        
       
        $( '#ime_korisnika' ).replaceWith( " <td id=\"ime_korisnika\"><input type=text id=\"defime\" placeholder=\"Your name\"></td>" );
        $( '#prezime_korisnika' ).replaceWith( " <td id=\"prezime_korisnika\"><input id=\"defprez\" type=text placeholder=\"Your surname\"></td>" );
        
        $( '#hobiji_korisnika' ).replaceWith( " <td id=\"hobiji_korisnika\"><input id=\"defhob\" type=text placeholder=\"Your hobbies\"></td>" );
   
        $("#prethodni").attr('class', 'col-md-9');
        
        
        $('#dugmad').append('<button type="button" class="btn btn-default btn-lg" id="save">'+
              '<span class="glyphicon glyphicon-floppy-open"></span> </button>'+
              '<button type="button" class="btn btn-default btn-lg" id="cancel">'+
              '<span class="glyphicon glyphicon-remove"></span> </button>');
        $('#dugmad').find('button:first').remove();
        
        $('#slicica').append('<a href="#" class="statusglyph"><input type="file" name="userfile" id="userfile"></a>');
        
//        switch(activetab) {
//                    case 'o_korisniku':
//                        $('#info_korisnik').remove(); 
//
//                        break;
//                    case 'pretplate':
//                        $('#info_pretplate').remove();  
//                        break;
//                    case 'komentari':
//                        $('#info_komentari').remove();  
//                        break;
//                }
          
       
       
    });
    
    $(document).on('click','#save', function(e) {
            e.preventDefault();
            var ime_korisnika = $('#defime').val();
            var prezime_korisnika = $('#defprez').val();
            var hobiji_korisnika = $('#defhob').val();
            var file = document.getElementById('userfile').files[0];
            if(file){
                $.ajaxFileUpload({
                type: 'post',
                url: 'profil/izmeni',
                data: {'ime_korisnika': ime_korisnika, 'prezime_korisnika': prezime_korisnika,'hobiji_korisnika': hobiji_korisnika },
                fileElementId: 'userfile',
                success: function(data, html) {
                            var status = data.activeElement.innerHTML;
                            $('#ovde').html(status);
                        }   
                });
            } else {
            
                $.ajax({
                type: 'POST',
                cache: false,
                data: {'ime_korisnika': ime_korisnika, 'prezime_korisnika': prezime_korisnika,'hobiji_korisnika': hobiji_korisnika },
                url: 'profil/izmeni',
                success: function(html) {
                        $('#ovde').html(html);
                   
                        }   
                });
            }            
        });
//        
        $(document).on('click','#cancel', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                cache: false,
                url: 'profil/brisi',
                success: function(html) {
                    $('#ovde').html(html);
                }
            });
         });
    
       

});
    