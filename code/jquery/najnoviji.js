$(document).ready(function() {
    var data = {'najnoviji_tip': 'oglas'};
    $.ajax({
        type: 'POST',
        data: data,
        url: 'najnoviji/load',
        cache: false,
        success: function(najnoviji) {
            $(najnoviji).hide().prependTo('#najnoviji').slideDown('slow');
        }
    });
    
    var dat = {'najnoviji_tip': 'file'};
    $.ajax({
        type: 'POST',
        data: dat,
        url: 'najnoviji/load',
        cache: false,
        success: function(najnoviji) {
            $(najnoviji).hide().prependTo('#najnoviji').slideDown('slow');
        }
    });
    
});