$(document).ready(function() {
    $('.subscribe').each(function() {
        $(this).on('click', function() {
            var predmet_id = $(this).parents('.collapse').attr('id');
            var predmet_container = $(this).parents('.panel-info');
            $.ajax({
                type: 'POST',
                data: {'predmet_id': predmet_id},
                url: 'predmet/subscribe',
                success: function(predmet) {
                    $(predmet_container).empty();
                    $(predmet_container).html($(predmet).html());
                    $(predmet_container).find('.panel-collapse').prop('class', 'panel-collapse collapse in');
                }
            });
        });
    });
    
    $('.unsubscribe').each(function() {
        $(this).on('click', function() {
            var predmet_id = $(this).parents('.collapse').attr('id');
            var predmet_container = $(this).parents('.panel-info');
            $.ajax({
                type: 'POST',
                data: {'predmet_id': predmet_id},
                url: 'predmet/unsubscribe',
                success: function(predmet) {
                    $(predmet_container).empty();
                    $(predmet_container).html($(predmet).html());
                    $(predmet_container).find('.panel-collapse').prop('class', 'panel-collapse collapse in');
                }
            });
        });
    });
    
});