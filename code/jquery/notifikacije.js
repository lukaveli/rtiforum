function poll() { 
    setTimeout(function() {
        $.ajax({
            type: 'post',
            url: 'proveravac/proveri',
            success: function(notifikacije) {
                if($(notifikacije).first().attr('class') === 'najnoviji_status_id') {
                    var status_id = $(notifikacije).first().attr('id');
                    var data = {'status_id': status_id};
                    $.ajax({
                        type: 'POST',
                        url: 'proveravac/najnoviji',
                        data: data,
                        dataType: 'json',
                        success: function(e) {
                            var notif = $(notifikacije).first().next();
                            
                            $(notif).prependTo('#notifikacije_container'); 
                                                      
                            $('#notifikacija_zvonce').find('sup').css('visibility', 'visible');
                           
                            $('#notifikacija_zvonce').find('span').css('color', 'red');
                            
                            var br = parseInt($('#notifikacija_zvonce').find('sup').text())
                                    + parseInt($(notifikacije).first().text());
                            $('#notifikacija_zvonce').find('sup').text(br);
                        }
                    });
                }
                poll();
            }
        });
    }, 5000);
}

function css(a) {
    var sheets = document.styleSheets, o = {};
    for (var i in sheets) {
        var rules = sheets[i].rules || sheets[i].cssRules;
        for (var r in rules) {
            if (a.is(rules[r].selectorText)) {
                o = $.extend(o, css2json(rules[r].style), css2json(a.attr('style')));
            }
        }
    }
    return o;
}

function css2json(css) {
    var s = {};
    if (!css) return s;
    if (css instanceof CSSStyleDeclaration) {
        for (var i in css) {
            if ((css[i]).toLowerCase) {
                s[(css[i]).toLowerCase()] = (css[css[i]]);
            }
        }
    } else if (typeof css == "string") {
        css = css.split("; ");
        for (var i in css) {
            var l = css[i].split(": ");
            s[l[0].toLowerCase()] = (l[1]);
        }
    }
    return s;
}

$(document).ready(function() {
    
    window.oldcss = css($('#notifikacija_zvonce').find('span'));
        
    $('#notifikacija_zvonce').on('click', function(e) {
        e.preventDefault();
        if($('.status_container').length > 0) {
            loadall_status(0, false, false);
        }
        $('#notifikacija_zvonce').find('sup').css('visibility', 'hidden');
        $('#notifikacija_zvonce').find('sup').text(0);
        
    });
    
    $('#notifikacija_zvonce').on('focusout', function() {
        $('#notifikacije_container').empty();
        $('#notifikacija_zvonce').find('span').css(window.oldcss);
    });
    
    poll();
});