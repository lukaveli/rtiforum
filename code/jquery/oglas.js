function deleteOglas(){
    $(document).find('.tab-content').find('.delete').on('click', function(e) {
        e.preventDefault();
        var status_id = $(this).parents('.status').attr('id');
        $.ajax({
           type: 'POST',
           url: 'oglas/obrisioglas',
           data: {'status_id': status_id},
           success: function(html) {
               var id='#' + status_id;
               $(id).remove();
           }
        });
    });
}

function setNewStatusProfileFunction(status_id){
    var id = '#' + status_id;
    $(id).find('.statusimglink').on('click', function(e) {
        e.preventDefault();
        $(document).find('.navbar').find('.active').removeClass();
        var korisnik_id = $(this).attr('id');
        $.ajax({
           type: 'POST',
           url: 'profil/profilishkec',
           data: {'korisnik_id': korisnik_id},
           success: function(html) {
               $(document).find('.row:first').remove();
               $(document).find('.row:first').html(html);
           }
        });
    });
}

function setStatusProfileFunction(statusi){
    statusi.find('.statusimglink').on('click', function(e) {
        e.preventDefault();
        $(document).find('.navbar').find('.active').removeClass();
        var korisnik_id = $(this).attr('id');
        $.ajax({
            type: 'POST',
            data: {'korisnik_id': korisnik_id},
            url: 'profil/profilishkec',
            success: function(html) {
                $(document).find('.row:first').remove();
                $(document).find('.row:first').html(html);
            }
        });
    });

}

function addTabFunctions() {
    $('#tipovi_oglasa').find('a').on('click', function() {
        var activetab = $(this).attr('href');
        activetab = activetab.replace('#', '');
        var tip = 0;
        switch(activetab) {
            case 'knjige':
                tip = 2;  
                break;
            case 'casovi':
                tip = 3;
                break;
            case 'domovi':
                tip = 4;
                break;
            case 'menza':
                tip = 5;
                break;
            case 'ostalo':
                tip = 6;
                break;
        }
        var data = {'tip': tip};
        $.ajax({
            type: 'POST',
            url: 'oglas/loadall',
            data: data,
            cache: false,
            success: function(status) {
                switch(activetab) {
                    case 'knjige':
                        $('#knjige_container').empty();
                        $('#knjige_container').prepend(status);
                        var statusi = $('#knjige_container').children('.status');
                        setStatusProfileFunction(statusi);
                        break;
                    case 'casovi':
                        $('#casovi_container').empty();
                        $('#casovi_container').prepend(status).slideDown('slow');
                        var statusi = $('#casovi_container').children('.status');
                        setStatusProfileFunction(statusi);
                        break;
                    case 'domovi':
                        $('#domovi_container').empty();
                        $('#domovi_container').prepend(status).slideDown('slow');
                        var statusi = $('#domovi_container').children('.status');
                        setStatusProfileFunction(statusi);
                        break;
                    case 'menza':
                        $('#menza_container').empty();
                        $('#menza_container').prepend(status).slideDown('slow');
                        var statusi = $('#menza_container').children('.status');
                        setStatusProfileFunction(statusi);
                        break;
                    case 'ostalo':
                        $('#ostalo_container').empty();
                        $('#ostalo_container').prepend(status).slideDown('slow');
                        var statusi = $('#ostalo_container').children('.status');
                        setStatusProfileFunction(statusi);
                        break;
                }
                deleteOglas();
            }
        });
        
    });
   
}

function init_forms() {
    $('form').submit(function(e) {
        e.preventDefault();
        var id = $(this).attr('id');
        var action=$(this).attr('action');
        
        if(id === 'postavi_form') {
            //vadimo koji je tab aktivan
            var activetab = $('#tipovi_oglasa').find('.active').children('a').attr('href');
            //oslobodimo se znaka '#'
            activetab = activetab.replace('#', '');           
            //odredjujemo tip oglasa
            var tip = 0;
            switch(activetab) {
                case 'knjige':
                    tip = 2;  
                    break;
                case 'casovi':
                    tip = 3;
                    break;
                case 'domovi':
                    tip = 4;
                    break;
                case 'menza':
                    tip = 5;
                    break;
                case 'ostalo':
                    tip = 6;
                    break;
            }
            
            var telo = $('#oglas_telo').val();
            var naslov = $('#oglas_naslov').val();
            var data = {'telo': telo, 'naslov': naslov, 'tip': tip};
            $.ajax({
                type: 'POST',
                url: action,
                data: data,
                cache: false,
                success: function(status) {
                    $('.statustextarea').val('');
                    switch(activetab) {
                        case 'knjige':
                            $(status).hide().prependTo('#knjige_container').slideToggle('slow');
                            break;
                        case 'casovi':
                            $('#casovi_container').prepend(status).slideDown('slow');  
                            break;
                        case 'domovi':
                            $('#domovi_container').prepend(status).slideDown('slow');  
                            break;
                        case 'menza':
                            $('#menza_container').prepend(status).slideDown('slow');  
                            break;
                        case 'ostalo':
                            $('#ostalo_container').prepend(status).slideDown('slow');  
                            break;
                    }
                    var status_id = $(status).attr('id');
                    setNewStatusProfileFunction(status_id);
                    deleteOglas();
                }
            });
        }
    });
}

function load_first_tab() {
    var tip = 2;
    var data = {'tip': tip};
    $.ajax({
        type: 'POST',
        url: 'oglas/loadall',
        data: data,
        cache: false,
        success: function(status) {
            $('#knjige_container').val('');
            $('#knjige_container').prepend(status).slideDown('slow');
            var statusi = $('#knjige_container').children('.status');
            setStatusProfileFunction(statusi);
            deleteOglas();
        }
    });
}


$(document).ready(function() {
    
    addTabFunctions();
    
    init_forms();
    
    load_first_tab();
        
});

