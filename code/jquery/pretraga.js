/* JS File */
function dodajKomentare(status_id){
        $.ajax({
                type: 'POST',
                data: {'status_id' : status_id},
                url: 'komentar/nadjeno',
                success: function(view){
                    $(document).find('.komentar_container').html(view);
                }
        });
}
// Start Ready
function addLinks(){
    $(document).find('.results').find('.searchresult').on('click',function(e){
                        e.preventDefault();
                        var status_id = $(this).attr('id');
                        var tip= $(this).find('span').attr('id');
                        $(document).find('.navbar').find('.active').removeClass();
                        var data = {'status_id' : status_id};
                        $.ajax({
                                type: 'POST',
                                data: data,
                                url: 'status/nadjeno',
                                success: function(view){
                                    $(document).find('.row').not(':last').empty();
                                    $(document).find('.row:first').html(view);
                                    if(tip==1) dodajKomentare(status_id);
                                }
                        });
                    });     
    
}


$(document).ready(function() {  
    

    $('#lupa').on('click', function(e) {

		e.preventDefault();
		$('.search').animate({width: 'toggle'}).focus();

    });
    
    $(document).find('.search').on('keypress',function(e){
        var code = e.keyCode || e.which;
        if(code === 13) {
            e.preventDefault();
            var string = $(this).find('input').val();
            var data = {'parametar': string};
            $.ajax({
                type: 'POST',
                data: data,
                url: 'search/findall',
                success: function(view){
                    $('.results').empty();
                    $(view).prependTo('.results');
                    addLinks();
                }
            });
        }
    });
    
    
  /*  
    
    // Icon Click Focus
	$('#lupa').click(function(){
            $('input#search_field').focus();
	});

	// Live Search
	// On Search Submit and Get Results
	function search() {
            var query_value = $('input#search_field').val();
            $('#search-string').html(query_value);
            if(query_value !== ''){
                $.ajax({
                    type: "POST",
                    url: "search.php",
                    data: { query: query_value },
                    cache: false,
                    success: function(html){
                        $("ul#results").html(html);
                    }
                });
            }
            return false;    
	}
        
	$("input#search_field").on("keydown", function(e) {
            // Set Timeout
            clearTimeout($.data(this, 'timer'));

            // Set Search String
            var search_string = $(this).val();

            // Do Search
            if (search_string == '') {
                $("ul#results").fadeOut();
                $('h4#results-text').fadeOut();
            }else{
                $("ul#results").fadeIn();
                $('h4#results-text').fadeIn();
                $(this).data('timer', setTimeout(search, 100));
            };
	});


*/


});