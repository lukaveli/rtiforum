function dele(){
    $(document).find('.table').find('tr').find('.del').on('click',function(e){
        e.preventDefault();
        var korisnik_id = $(this).parents('tr').attr('id');
        $.ajax({
           type: 'POST',
           url: 'panel/obrisi',
           data: {'korisnik_id': korisnik_id},
           success: function(html) {
               var id='#' + korisnik_id;
               $(id).remove();
           }
        });
    });
}

function promo(){
    $(document).find('.table').find('tr').find('.promote').on('click',function(e){
        e.preventDefault();
        var korisnik_id = $(this).parents('tr').attr('id');
        $.ajax({
           type: 'POST',
           url: 'panel/promo',
           data: {'korisnik_id': korisnik_id},
           success: function(html) {
               var id='#' + korisnik_id;
               $(id).find('.promote').replaceWith("<td >ADMIN</td>");
               $(id).find('.del').replaceWith("<td >IMPOSSIBRU</td>");
           }
        });
    });
}

$(document).ready(function() {
    promo();
    dele();
});


