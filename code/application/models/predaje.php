<?php

class Predaje extends MY_Model {
    const DB_TABLE = 'predaje';
    const DB_TABLE_PK = 'korisnik_id, predmet_id';
    const DB_TABLE_PK1 = 'korisnik_id';
    const DB_TABLE_PK2 = 'predmet_id';
    
    /**
     * @var int
     */
    public $korisnik_id;
    
    /**
     * @var int
     */
    public $predmet_id;
    
    public function load($id1, $id2) {
        $query = $this->db->get_where($this::DB_TABLE, array(
           'korisnik_id' => $id1, 'predmet_id' => $id2,
        ));
        $this->populate($query->row());
    }
    
    public function delete() {
        $this->db->delete($this::DB_TABLE, array(
            $this::DB_TABLE_PK1 => $this->{$this::DB_TABLE_PK1},
            $this::DB_TABLE_PK2 => $this->{$this::DB_TABLE_PK2},
        ));
        unset($this->{$this::DB_TABLE_PK});
    }
        
}