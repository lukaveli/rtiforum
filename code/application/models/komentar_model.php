<?php

class Komentar_Model extends MY_Model {
    const DB_TABLE = 'komentar';
    const DB_TABLE_PK = 'komentar_id';
    
    /**
     * @var int
     */
    public $komentar_id;
    
    /**
     * @var int
     */
    public $korisnik_id;
    
    /**
     * @var int
     */
    public $status_id;
    
    /** 
    * @var string
    */ 
    public $komentar_datum;
    
    /** 
    * @var string
    */ 
    public $komentar_telo;

}