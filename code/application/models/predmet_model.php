<?php

class Predmet_Model extends MY_Model {
    const DB_TABLE = 'predmet';
    const DB_TABLE_PK = 'predmet_id';
    
    /**
     * @var int
     */
    public $predmet_id;
    
    /** 
    * @var string
    */
    public $predmet_ime;
    
    /** 
    * @var string
    */ 
    public $predmet_skracenica;

    /** 
    * @var string
    */ 
    public $predmet_opis;
    
}