<?php

class Dislajkovao_Model extends MY_Model {
    const DB_TABLE = 'dislajkovao';
    const DB_TABLE_PK = 'dislajkovao_id';
    
    /**
     * ovo nije ni potrebno
     * @var int
     */
    public $dislajkovao_id;
       
    /**
     * @var int
     */
    public $korisnik_id;
    
    /**
     * @var int
     */
    public $status_id;
           
}

