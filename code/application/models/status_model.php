<?php

class Status_Model extends MY_Model {
    const DB_TABLE = 'status';
    const DB_TABLE_PK = 'status_id';
    
    /**
     * @var int
     */
    public $status_id;
    
    /**
     *@var int
     */
    public $predmet_id;
    
    /**
     *@var int
     */
    public $korisnik_id;
    
    /** 
    * @var string
    */ 
    public $status_telo;
    
    /** 
    * @var string
    */ 
    public $status_naslov;
    
    /** 
    * @var string
    */ 
    public $status_datum;
    
    /**
     *@var int
     */
    public $status_br_lajkova;
    
    /**
     *@var int
     */
    public $status_br_dislajkova;
    
    /** 
    * @var string
    */ 
    public $status_attachment;
    
    /**
     *@var int
     */
    public $status_tip;
    
    /**
     *@var int
     */
    public $status_hidden;
    
}

