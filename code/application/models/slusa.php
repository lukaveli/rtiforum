<?php

class Slusa extends MY_Model {
    const DB_TABLE = 'slusa';
    const DB_TABLE_PK = 'korisnik_id, predmet_id';
    
    /**
     * @var int
     */
    public $korisnik_id;
    
    /**
     * @var int
     */
    public $predmet_id;
    
    /** 
    * @var string
    */
    public $slusa_utisak;
        
}