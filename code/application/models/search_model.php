<?php

class Search_Model extends MY_Model {
    const DB_TABLE = 'search';
    const DB_TABLE_PK = 'search_id';
    
    /**
     * @var int
     */
    public $search_id;
       
    /** 
    * @var string 
    */
    public $search_function;
           
    /** 
    * @var string 
    */
    public $search_name;
    
}