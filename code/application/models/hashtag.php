<?php

class Hashtag extends MY_Model {
    const DB_TABLE = 'hashtag';
    const DB_TABLE_PK = 'hashtag_id';
    
    /**
     * @var int
     */
    public $hashtag_id;
       
    /** 
    * @var string 
    */
    public $hashtag_tekst;
        
}