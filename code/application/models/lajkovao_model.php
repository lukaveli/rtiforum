<?php

class Lajkovao_Model extends MY_Model {
    const DB_TABLE = 'lajkovao';
    const DB_TABLE_PK = 'lajkovao_id';
    
    /**
     * ovo nije ni potrebno
     * @var int
     */
    public $lajkovao_id;
       
    /**
     * @var int
     */
    public $korisnik_id;
    
    /**
     * @var int
     */
    public $status_id;
           
}

