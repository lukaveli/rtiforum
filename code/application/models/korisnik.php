<?php

class Korisnik extends MY_Model {
    const DB_TABLE = 'korisnik';
    const DB_TABLE_PK = 'korisnik_id';
    
    /**
     * @var int
     */
    public $korisnik_id;
    
    /**
     *@var int
     */
    public $korisnik_tip;
    
    /** 
    * @var string
    */ 
    public $korisnik_ime;
    
    /** 
    * @var string
    */ 
    public $korisnik_prezime;
    
    /** 
    * @var string
    */ 
    public $korisnik_email;
    
    /** 
    * @var string
    */ 
    public $korisnik_password;
    
    /** 
    * @var string
    */ 
    public $korisnik_joined;
    
    /** 
    * @var string
    */ 
    public $korisnik_hobbies;
    
    /** 
    * @var string
    */ 
    public $korisnik_slika;
    
    /** 
    * @var string
    */
    public $korisnik_datum_rodjenja;
    
    /**
     *@var int
     */
    public $korisnik_admin;
}
