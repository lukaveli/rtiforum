<div class="panel panel-info">
    
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="<?php echo "#$predmet->predmet_id"; ?>">
          <?php echo "$predmet->predmet_ime [$predmet->predmet_skracenica]"; ?>
        </a>
      </h4>
    </div>
    
    <div id="<?php echo $predmet->predmet_id; ?>" class="panel-collapse collapse">
        <div class="panel-body">
                <?php echo "$predmet->predmet_opis"; ?>
                <br />
                <?php 
                    if($to_subscribe) {
                        echo "<button type='button' class='btn btn-primary pull-right subscribe'>Subscribe</button>";
                    } else {
                        echo "<button type='button' class='btn btn-warning pull-right unsubscribe'>Unsubscribe</button>";
                    }
                ?>
          </div>
    </div>
</div>