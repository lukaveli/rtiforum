<div class="forma_o_korisniku" id="forma_o_korisniku"<?php echo $status_id; ?>">  
    <div class="caption">

        <!--slika-->
        <div class="pull-left">
          <div class="statusimglink">
            <a class="thumbnail" href="#">
              <img src="<?php echo $slika; ?>" alt="..." class="statusimg">
            </a>
          </div>
        </div><!--slika-->

        <!--ime i datum-->
        <h3 class="statusheader">
          <?php echo "$ime $prezime";?>
          <small>
            <p class="statusdate">
              <small> <?php echo $datum;?></small>

            </p>
          </small>
        </h3><!--ime i datum-->          

        <!--text-->
        <div class="well">
        <p><?php echo $telo;?></p>
        </div><!--text-->

        <!--like i dislike i komentar forma-->
        <div class="row">

            <div class="col-md-2">
                <center class="lajkdislajk">
                    
                    <button type="button" class="btn btn-success btn-sm lajk">
                        <span class="glyphicon glyphicon-thumbs-up"></span>
                    </button>
                    <button type="button" class="btn btn-danger btn-sm dislajk">
                        <span class="glyphicon glyphicon-thumbs-down"></span>
                    </button>
                    <h4>
                        <font class="br_lajkova" color="green" align="left"><?php echo $br_lajkova?></font>
                        <font class="br_dislajkova" color="red"><?php echo $br_dislajkova?></font>
                    </h4>
                </center>
            </div>

            <div class="col-md-10">
                <textarea class="form-control statustextarea" rows="2"></textarea>
            </div>

        </div><!--like i dislike i komentar-->

    </div>
</div><!--status-->
