<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url('home');?>">RTI Forum</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            
            <div class="col-md-4">
                <ul class="nav navbar-nav">
                    <li class="<?php echo $kuca_active; ?>"><a href="<?php echo site_url('home');?>">Kuća</a></li>
                    <li class="<?php echo $predmeti_active; ?>">
                        <a href="#" cla   ss="dropdown-toggle" data-toggle="dropdown">Predmeti <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url('predmet');?>">Prva godina</a></li>
                            <li><a href="<?php echo site_url('predmet#druga');?>">Druga godina</a></li>
                            <li><a href="<?php echo site_url('predmet#treca');?>">Treća godina</a></li>
                            <li><a href="<?php echo site_url('predmet#cetvrta');?>">Četvrta godina</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo site_url('predmet#master');?>">Master studije</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo site_url('predmet#doktorske');?>">Doktorske studije</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo $oglasi_active; ?>"><a href="<?php echo site_url('oglas');?>">Oglasi</a></li>
                    <?php if($korisnik_admin == 1)
                        echo '<li class="'.$panel_active.'"><a href="panel">Admin panel</a></li>';
                    ?>
                </ul><!--Linkovi-->
            </div>
            
            <div class="col-md-6">
                <div class="pull-right">
                    <ul class="nav navbar-nav">
                        <li>
                            <form class="search" method="post" action="index.html" hidden="true" >
                                    <input type="text" name="q" placeholder="Search..." />
                                    <ul class="results" >
                                           <!-- rezultati -->
                                    </ul>
                            </form>
                        </li>
                        <li><a href="#" id="lupa"><span class="glyphicon glyphicon-search"></span></a></li>
                       
                        <li class="dropdown">
                            
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="notifikacija_zvonce">
                                <span class="glyphicon glyphicon-bell"></span>
                                <sup class="br_notif">0</sup>
                            </a>
                            
                            <ul class="dropdown-menu dropdown-menu-right" id="notifikacije_container">
                                
                            </ul>
                            
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle profilnacontainer" data-toggle="dropdown">
                                <?php echo "<img class='profilnaslika' src='$slika' alt='profilna' height='18' width='18'>"?>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('profil');?>"><span class="glyphicon glyphicon-user"></span> &nbsp;Profil</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url();?>home/logout"><span class="glyphicon glyphicon-log-out"></span> &nbsp;Logout</a></li>
                            </ul>
                        </li>
                    </ul><!--profil notifikacije-->
                </div>
            </div>
            
        </div>
        
    </div>
</nav><!--navbar-->


