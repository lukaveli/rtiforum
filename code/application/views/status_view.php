<div class="thumbnail status <?php if($last) echo 'last';?>" id="<?php echo $status_id; ?>">
    <?php if($korisnik_admin == 1)
        echo '<div class="caption" ><a class="delete" href="#">  Delete</a></div>';
    ?>
    <div class="caption">

        <!--slika-->
        <div class="pull-left">
          <div class="statusimglink" id="<?php echo $korisnik_id; ?>">
            <a class="thumbnail" href="#">
              <img src="<?php echo $slika; ?>" alt="<?php echo "$ime $prezime"; ?>" class="statusimg">
            </a>
          </div>
        </div><!--slika-->
        
        <div class="pull-right">
            <label id="download_name">
                <?php
                    if($attachment !== 'no_file' && $attachment !== NULL) { 
                        echo $attachment;
                    }
                ?>
            </label>
            <a href="#" class="statusglyph" id="download_file">
                <?php 
                    if($attachment !== 'no_file' && $attachment !== NULL) {
                        echo "<span class='glyphicon glyphicon-paperclip'></span>";
                    }
                ?>
            </a>
        </div>

        <!--ime i datum-->
        <h3 class="statusheader">
          <?php echo "$ime $prezime";?>
          <small>
            <p class="statusdate">
                <small> <?php echo $datum;?></small>
            </p>
          </small>
        </h3><!--ime i datum-->

        <!--text-->
        <div class="well status_telo">
            <p>
                
                <div class="hashtag">
                    <?php echo "[#$skracenica]"; ?>
                </div>
                
                <?php echo $telo; ?>
            </p>
        </div><!--text-->

        <!--like i dislike i komentar forma-->
        <div class="row">

            <div class="col-md-2">
                <center class="lajkdislajk">
                    
                    <button type="button" class="btn btn-success btn-sm lajk">
                        <span class="glyphicon glyphicon-thumbs-up"></span>
                    </button>
                    <button type="button" class="btn btn-danger btn-sm dislajk">
                        <span class="glyphicon glyphicon-thumbs-down"></span>
                    </button>
                    <h4>
                        <font class="br_lajkova" color="green" align="left"><?php echo $br_lajkova?></font>
                        <font class="br_dislajkova" color="red"><?php echo $br_dislajkova?></font>
                    </h4>
                </center>
            </div>

            <div class="col-md-10">
                <textarea id="komentararea" name="telo" class="form-control statustextarea" rows="1"></textarea>
            </div>

        </div><!--like i dislike i komentar-->

    </div>
</div><!--status-->

<!-- ovde se dodaju komentari -->
<div class="komentar_container">
    
</div>
