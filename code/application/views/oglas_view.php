<div class="thumbnail status" id="<?php echo $status_id; ?>">
    <?php if($korisnik_admin == 1)
        echo '<div class="caption" ><a class="delete" href="#">  Delete</a></div>';
    ?>
    
    <div class="caption">

        <!--slika-->
        <div class="pull-left">
          <div class="statusimglink" id="<?php echo $korisnik_id;?>">
            <a class="thumbnail" href="#">
              <img src="<?php echo $slika?>" alt="..." class="statusimg" >
            </a>
          </div>
        </div><!--slika-->

        <!--ime i datum-->
        <h3 class="statusheader">
        <?php echo $naslov;?>
        <br />
          <small>
            <?php echo "$ime $prezime";?>
            <p class="statusdate">
              <small> <?php echo $datum;?></small>

            </p>
          </small>
        </h3><!--ime i datum-->          

        <!--text-->
        <div class="well">
        <p><?php echo $telo;?></p>
        </div><!--text-->


    </div>
</div><!--status-->
