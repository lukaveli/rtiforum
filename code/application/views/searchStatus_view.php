<div class="row">
<div class="col-md-7">
<div class="thumbnail status" id="<?php echo $status_id; ?>">  
    <div class="caption">

        <!--slika-->
        <div class="pull-left">
          <div class="statusimglink">
            <a class="thumbnail" href="#">
              <img src="<?php echo $slika; ?>" alt="<?php echo "$ime $prezime"; ?>" class="statusimg">
            </a>
          </div>
        </div><!--slika-->
        

        <!--ime i datum-->
        <h3 class="statusheader">
          <?php echo "$ime $prezime";?>
          <small>
            <p class="statusdate">
                <small> <?php echo $datum;?></small>
            </p>
          </small>
        </h3><!--ime i datum-->

        <!--text-->
        <div class="well status_telo">
            <p>
                
                <div class="hashtag">
                    <?php echo "[$skracenica]"; ?>
                </div>
                
                <?php echo $telo; ?>
            </p>
        </div><!--text-->

        <!--like i dislike i komentar forma-->

    </div>
</div><!--status-->

<!-- ovde se dodaju komentari -->
<div class="komentar_container">
    
</div>
</div>
</div>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

