<div id="ovde">
<div class="row">
  <div class="col-md-1">
  </div>
  <div class="col-md-10">
      <ul class="breadcrumb">
          <li><a href="<?php echo site_url('home');?>">RTI Forum</a></li>
          <li class="active">Moj profil</li>
      </ul>
  </div>
  <div class="col-md-1">
  </div>
</div><!--breadcrumbs-->

<div class="row">
    <div class="col-md-1">
    </div>

    <div id="slicica" class="col-md-3">
            <div class="thumbnail">
                    <img src="<?php echo $profilna_slika;?>" alt="profilna">
                    <div class="caption">
                            <h3 align=center><?php echo "$ime $prezime";?></h3>
                    </div>
            </div>
    </div>
    <div class="col-md-7">
        <ul class="nav nav-tabs" id="tabovi">
                <li class="active"><a href="#o_korisniku"  data-toggle="tab" class="active" id="korisnik_tab">O korisniku</a></li>
                <li><a href="#pretplate" data-toggle="tab" id="pretplate_tab"> Pretplate</a></li>
                <li><a href="#komentari" data-toggle="tab" id="komentari_tab"> Komentari</a></li>
        </ul>


        <div class="tab-content">
            <div class="tab-pane fade active in" id="o_korisniku">
                <p></p>
                <table class="table table-striped" id="info_korisnik">
                        <tr >
                                <td class="col-md-2">Ime:</td>
                                <td id="ime_korisnika"><?php echo "$ime";?></td>
                        </tr>
                        <tr >
                                <td class="col-md-2">Prezime:</td>
                                <td id="prezime_korisnika"><?php echo "$prezime";?></td>
                        </tr>
                        <tr >
                                <td class="col-md-2">E-mail:</td>
                                <td> <?php echo "$korisnik_email";?></td>
                        </tr>
                        <tr >
                                <td class="col-md-2">Joined:</td>
                                <td><?php echo "$korisnik_joined";?></td>
                        </tr>
                        <tr >
                                <td class="col-md-2">Hobi:</td>
                                <td id="hobiji_korisnika"><?php echo "$korisnik_hobbies";?></td>
                        </tr>
                </table>
            </div>

             <!-- Checkups -->
            <div class="tab-pane fade" id="pretplate">
                <p></p>
                <div id="info_pretplate" >
                    <?php
                        foreach($pretplate as $pretplata){
                            echo '<table class="table table-bordered">'
                                    .'<tr >'.
                                        '<td class="info">'.html_escape($pretplata['predmet_skracenica']).'</td>'
                                    .'</tr>'
                                    .'<tr >'
                                        .'<td>'.html_escape($pretplata['predmet_opis']).'</td>'
                                    .'</tr>'
                                  .'</table>';
                        }
                    ?>
<!--                <table class="table table-bordered">
                        <tr >
                                <td class="info"><a href="si3kdp.php">SI3KDP</a></td>
                        </tr>
                        <tr >
                                <td>Kurs se pohadja u 6. semestru Osnovnih akademskih studija. Bavi se izucavanjem konkurentno distrubuiranog programiranja i raznih nekih stvari</td>
                        </tr>
                </table>
                <table class="table table-bordered">
                        <tr >
                                <td class="info"><a href="si3ps.php">SI3PS</a></td>
                        </tr>
                        <tr >
                                <td>Kurs se pohadja u 6. semestru Osnovnih akademskih studija. Bavi se izucavanjem projektovanjem softvera programiranja i raznih nekih stvari</td>
                        </tr>
                </table>-->
                </div>
            </div>

            <!-- Senior Pets -->
           <div class="tab-pane fade" id="komentari">
               <p></p>
               <table class="table table-bordered" id="info_komentari">
                    <?php
                        foreach($komentari as $komentar){
                            echo '<tr>'
                                    .'<td class="info">'.html_escape($komentar['komentar_datum']).'</td>'
                                    .'<td>'.html_escape($komentar['komentar_telo']).'</td>'
                                  .'</tr>';
                        }
                    ?>
<!--                    <tr >
                            <td class="info">SI3KDP:</td>
                            <td>Pohadjanje kursa u toku</td>
                    </tr>
                    <tr >
                            <td class="info">SI2OO2:</td>
                            <td>Redovan rad i dobri rezultati na predizbornim obavezama olaksavaju ispit</td>
                    </tr>
                    <tr >
                            <td class="info">SI3PS:</td>
                            <td>Kao i OO2</td>
                    </tr>-->
               </table>							
           </div>

        </div>
        
    </div>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div id="prethodni" class="col-md-10">
            <div id="proba">
                
            </div>
        </div>
        <div id="dugmad" class="col-md-2">
            <!--
            <?php
               echo "<button type='button' class='btn btn-default btn-lg'>";
               echo "<span class='glyphicon glyphicon-pencil'></span>";
               echo "</button>"; 
            ?>  
            -->
            <button type='button' class='btn btn-default btn-lg' id="edit">
              <span class='glyphicon glyphicon-pencil'></span>
               
            </button>
        </div>
        
    </div>
</div>
</div>
  

