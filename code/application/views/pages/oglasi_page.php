<div class="row">
  <div class="col-md-1">
  </div>
  <div class="col-md-10">
      <ul class="breadcrumb">
          <li><a href="<?php echo site_url('home');?>">RTI Forum</a></li>
          <li><a href="<?php echo site_url('home');?>">Kuca</a></li>
          <li class="active">Oglasi</li>
      </ul>
  </div>
  <div class="col-md-1">
  </div>
</div><!--breadcrumbs-->

  <div class="row">
    <div class="col-md-1">
    </div>

    <!--GLAVNI DEO-->

    <div class="col-md-7">
      <!--dodavanje statusa-->

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Napravi svoj oglas</h3>
            </div>                
                  
            <div class="panel-body">
                <?php echo validation_errors()?>
                <?php 
                    $url = 'oglas/add';
                    $attributes = array('role' => 'form', 'id' => 'postavi_form');
                    echo form_open($url, $attributes); 
                ?>
                
                <label>Naslov</label>
                <?php
                    $attributes = array (
                        'name' => 'naslov',
                        'id' => 'oglas_naslov',
                        'class' => 'form-control statustextarea',
                        'rows' => '1',
                    );
                    echo form_textarea($attributes)
                ?>
                
                <label>Oglas</label>
                <?php
                    $attributes = array (
                        'name' => 'telo',
                        'id' => 'oglas_telo',
                        'class' => 'form-control statustextarea',
                        'rows' => '3',
                    );
                    echo form_textarea($attributes)
                ?>
               
                <div class="pull-right">
                    <?php
                        $attributes = array (
                            'type' => 'submit',
                            'name' => 'postavi',
                            'value' => 'Postavi',
                            'id' => 'postavi',
                            'class' => 'btn btn-danger',
                        );
                        echo form_submit($attributes)
                    ?>
                </div>
                
                <?php echo form_close()?>
            </div>
        </div><!--dodavanje statusa-->    
      
        <ul class="nav nav-tabs" id="tipovi_oglasa">
            <li class="active"><a href="#knjige" data-toggle="tab" id="knjige_tab">Prodaja knjiga</a></li>
            <li><a href="#casovi" data-toggle="tab" id="casovi_tab">Privatni casovi</a></li>
            <li><a href="#domovi" data-toggle="tab" id="domovi_tab">Mesta u domovima</a></li>
            <li><a href="#menza" data-toggle="tab" id="menza_tab">Kartice za menzu</a></li>
            <li><a href="#ostalo" data-toggle="tab" id="ostalo_tab">Ostalo</a></li>
        </ul>
		
        <div class="tab-content">
            <!-- Prodaja knjiga tip statusa = 2 -->
            <div class="tab-pane fade active in" id="knjige">                
                <div id="knjige_container">
            
                </div>      
            </div>
    
             <!-- Privatni casovi tip statusa = 3 -->
            <div class="tab-pane fade" id="casovi">
		<div id="casovi_container">
            
                </div>
            </div>
    
             <!-- Mesto u domovima tip statusa = 4 -->
            <div class="tab-pane fade" id="domovi">
 		 <div id="domovi_container">
            
                </div>          
            </div>
    
            <!-- Kartice za menzu tip statusa = 5 -->
            <div class="tab-pane fade" id="menza">
               <div id="menza_container">
            
                </div> 
            </div>
            
            <!-- Ostalo tip statusa = 6 -->
            <div class="tab-pane fade" id="ostalo">
               <div id="ostalo_container">
            
                </div> 
            </div>
        </div>

    </div>

    <div class="col-md-3" id="najnoviji">
           
    </div>

    <div class="col-md-1">
    </div>

  </div><!--row 1-->

  