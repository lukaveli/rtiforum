<div class="row">
    <div class="col-md-1">
    </div>
    <div class="col-md-10">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('home');?>">RTI Forum</a></li>
            <li class="active">Kuca</li>
        </ul>
    </div>
    <div class="col-md-1">
    </div>
</div><!--breadcrumbs-->

<div class="row">
    <div class="col-md-1">
    </div>

    <!--GLAVNI DEO-->
    
    <div class="col-md-7">
        <div id="validation_errors">

        </div>
        <!--forma za dodavanje statusa-->
        <div class="panel panel-default">
            <div class="panel-heading">Dodaj nesto</div>
            <div class="panel-body">
                
                <?php 
                    $url = 'status/add';
                    $attributes_form = array('role' => 'form', 'id' => 'postavi_form');
                    echo form_open_multipart($url, $attributes_form); 
                ?>
                                
                <?php
                    $attributes_textarea = array (
                        'name' => 'telo',
                        'id' => 'status_textarea',
                        'class' => 'form-control statustextarea',
                        'rows' => '3',
                    );
                    echo form_textarea($attributes_textarea)
                ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-tag"></span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu nav nav-stacked" role="menu" id="predmeti_odabir">
                        <?php
                            $i = 0;
                            foreach($predmeti as $predmet) {
                                $id = $predmet['id'];
                                $skracenica = $predmet['skracenica'];
                                if($skracenica !== 'Info') {
                                    echo "<li class='notactive' id='$id'><a data-toggle='tab' href='#$skracenica'>$skracenica</a></li>";
                                } else {
                                    if($i != 0) {
                                        echo "<li role='presentation' class='divider'></li>";
                                    }
                                    echo "<li class='active' id='$id'><a data-toggle='tab' href='#$skracenica'>$skracenica</a></li>";
                                } 
                                $i++;
                            }
                            
                        ?>
                    </ul>
                </div>

                <a href="#" class="statusglyph">
                    <input type="file" name="userfile" id="userfile">
                    <span class="glyphicon glyphicon-paperclip" id="upload_file"></span>
                </a>
                
                <label id="filename"></label>

                <div class="pull-right">
                    <?php
                        $attributes_submit = array (
                            'type' => 'submit',
                            'name' => 'postavi',
                            'value' => 'Postavi',
                            'id' => 'postavi',
                            'class' => 'btn btn-danger',
                        );
                        echo form_submit($attributes_submit)
                    ?>
                </div>
                
                <?php echo form_close()?>
            </div>
        </div><!--forma za dodavanje statusa-->    
        
        <!--ovde ucitavamo statuse-->
        <div class="status_container">

        </div>
        
    </div>

    <div class="col-md-3" id="najnoviji">
           
    </div>

    <div class="col-md-1">
    </div>

</div><!--row 1-->