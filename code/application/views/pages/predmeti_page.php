<div class="row">
  <div class="col-md-1">
  </div>
  <div class="col-md-10">
      <ul class="breadcrumb">
          <li><a href="<?php echo site_url('home');?>">RTI Forum</a></li>
          <li><a href="<?php echo site_url('home');?>">Kuca</a></li>
          <li class="active">Predmeti</li>
      </ul>
  </div>
  <div class="col-md-1">
  </div>
</div><!--breadcrumbs-->

  <div class="row">
    <div class="col-md-1">
    </div>

    <!--GLAVNI DEO-->

    <div class="col-md-7">
        <div class="page-header">
            <h1>Prva godina</h1>
        </div>
        <div class="panel-group" id="accordion">
            <?php 
                foreach($predmeti as $predmet) {
                    if(strpos($predmet->predmet_skracenica, "SI1") === 0 || strpos($predmet->predmet_skracenica, 'OO1') === 0) {
                        if(isset($subscribed[$predmet->predmet_id])) {
                            $to_subscribe = FALSE;
                        } else {
                            $to_subscribe = TRUE;
                        }
                        $data = array('predmet' => $predmet, 'to_subscribe' => $to_subscribe);
                        $this->load->view('predmet_view', $data);
                    }
                }
            ?>
        </div>
        
        <div class="page-header" id="druga">
            <h1>Druga godina</h1>
        </div>
        <div class="panel-group" id="accordion">
            <?php 
                foreach($predmeti as $predmet) {
                    if(strpos($predmet->predmet_skracenica, "SI2") === 0 || strpos($predmet->predmet_skracenica, 'IR2') === 0) {
                        if(isset($subscribed[$predmet->predmet_id])) {
                            $to_subscribe = FALSE;
                        } else {
                            $to_subscribe = TRUE;
                        }
                        $data = array('predmet' => $predmet, 'to_subscribe' => $to_subscribe);
                        $this->load->view('predmet_view', $data);
                    }
                }
            ?>
        </div>
        
        <div class="page-header" id="treca">
            <h1>Treća godina</h1>
        </div>
        <div class="panel-group" id="accordion">
            <?php 
                foreach($predmeti as $predmet) {
                    if(strpos($predmet->predmet_skracenica, "SI3") === 0 || strpos($predmet->predmet_skracenica, 'IR3') === 0) {
                        if(isset($subscribed[$predmet->predmet_id])) {
                            $to_subscribe = FALSE;
                        } else {
                            $to_subscribe = TRUE;
                        }
                        $data = array('predmet' => $predmet, 'to_subscribe' => $to_subscribe);
                        $this->load->view('predmet_view', $data);
                    }
                }
            ?>
        </div>
        
        <div class="page-header" id="cetvrta">
            <h1>Četvrta godina</h1>
        </div>
        <div class="panel-group" id="accordion">
            <?php 
                foreach($predmeti as $predmet) {
                    if(strpos($predmet->predmet_skracenica, "SI4") === 0 || strpos($predmet->predmet_skracenica, 'IR4') === 0) {
                        if(isset($subscribed[$predmet->predmet_id])) {
                            $to_subscribe = FALSE;
                        } else {
                            $to_subscribe = TRUE;
                        }
                        $data = array('predmet' => $predmet, 'to_subscribe' => $to_subscribe);
                        $this->load->view('predmet_view', $data);
                    }
                }
            ?>
        </div>
        
        <div class="page-header" id="master">
            <h1>Master studije</h1>
        </div>
        <div class="panel-group" id="accordion">
            <?php 
                foreach($predmeti as $predmet) {
                    if(strpos($predmet->predmet_skracenica, "MR") === 0) {
                        if(isset($subscribed[$predmet->predmet_id])) {
                            $to_subscribe = FALSE;
                        } else {
                            $to_subscribe = TRUE;
                        }
                        $data = array('predmet' => $predmet, 'to_subscribe' => $to_subscribe);
                        $this->load->view('predmet_view', $data);
                    }
                }
            ?>
        </div>
        
        <div class="page-header" id="doktorske">
            <h1>Doktorske studije</h1>
        </div>
        <div class="panel-group" id="accordion">
            <?php 
                foreach($predmeti as $predmet) {
                    if(strpos($predmet->predmet_skracenica, "DR") === 0) {
                        if(isset($subscribed[$predmet->predmet_id])) {
                            $to_subscribe = FALSE;
                        } else {
                            $to_subscribe = TRUE;
                        }
                        $data = array('predmet' => $predmet, 'to_subscribe' => $to_subscribe);
                        $this->load->view('predmet_view', $data);
                    }
                }
            ?>
        </div>
        
    </div>
    <div class="col-md-3" id="najnoviji">  
        
    </div>

    <div class="col-md-1">
    </div>

  </div><!--row 1-->

  