<nav class="navbar navbar-inverse navbar-fixed-top navbar-inverted" role="navigation">
    <div class="container">
        <a class="navbar-brand" href="">RTI Forum</a>
    </div>
</nav>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="jumbotron">
          <div class="container">
            <div class="caption">
              <center><h2>Welcome to</h2></center>
              <center><h1>RTI Forum</h1></center>
            </div>
          </div>
        </div><!-- .hero-unit -->
    </div>
    <div class="col-md-2"></div>
</div>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-3">
        <div class="jumbotron" id="loginjumbo">
            <div class="container">
                <?php echo validation_errors(); ?>
                <?php
                    $url = "welcome/verify_login";
                    $attributes = array('role' => 'form');
                    echo form_open($url, $attributes); 
                ?>
                <div class="form-group">
                    <?php
                        $attributes = array(
                                            'type' => 'email',
                                            'class' => 'form-control',
                                            'name' => 'email',
                                            'placeholder' => 'Enter email',
                                            );
                        echo form_input($attributes); 
                    ?>
                </div>
                <div class="form-group">
                    <?php
                        $attributes = array(
                                            'type' => 'password',
                                            'class' => 'form-control',
                                            'name' => 'password',
                                            'placeholder' => 'Password',
                                            );
                        echo form_input($attributes); 
                    ?>
                </div>
                <div class="pull-right">
                    <?php
                        $attributes = array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-primary',
                                            'id' => 'logintest',
                                            'value' => 'Login',
                                            );
                        echo form_submit($attributes); 
                    ?>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div><!-- .hero-unit -->
    </div>
    <div class="col-md-5">
        <div class="jumbotron" id="registerjumbo">
            <div class="container">
                <?php echo validation_errors(); ?>
                <?php
                    $url = "welcome/verify_register";
                    $attributes = array('role' => 'form');
                    echo form_open($url, $attributes); 
                ?>
                <div class="form-group">
                    <?php
                        $attributes = array(
                                            'type' => 'email',
                                            'class' => 'form-control',
                                            'name' => 'email',
                                            'placeholder' => 'ETF Student email',
                                            );
                        echo form_input($attributes); 
                    ?>
                </div>
                <div class="form-group">
                    <?php
                        $attributes = array(
                                            'type' => 'password',
                                            'class' => 'form-control',
                                            'name' => 'password',
                                            'placeholder' => 'Password',
                                            );
                        echo form_input($attributes); 
                    ?>
                </div>
                <div class="form-group">
                    <?php
                        $attributes = array(
                                            'type' => 'password',
                                            'class' => 'form-control',
                                            'name' => 'password_confirm',
                                            'placeholder' => 'Confirm password',
                                            );
                        echo form_input($attributes); 
                    ?>
                </div>
                <div class="form-group">
                    <label class="sex">Sex: </label>
                    
                   
                    <label class="sex">
                        <?php
                            $attributes = array(
                                                'type' => 'radio',
                                                'id' => 'male',
                                                'name' => 'sex',
                                                'value' => 'Male',
                                                );
                            echo form_input($attributes); 
                        ?>
                        Male
                    </label>
                   
                    <label class="sex">
                        <?php
                            $attributes = array(
                                                'type' => 'radio',
                                                'id' => 'female',
                                                'name' => 'sex',
                                                'value' => 'Female',
                                                );
                            echo form_input($attributes); 
                        ?>
                        Female
                    </label>
                  
                    <label>
                        <?php
                            $attributes = array(
                                                'type' => 'radio',
                                                'id' => 'savic',
                                                'name' => 'sex',
                                                'value' => 'Savic',
                                                'checked' => 'true',
                                                );
                            echo form_input($attributes); 
                        ?>
                        Savic
                    </label>
                    
                </div>
                <div class="pull-right">
                    <?php
                        $attributes = array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-warning',
                                            'value' => 'Register',
                                            );
                        echo form_submit($attributes); 
                    ?>
                </div>
                <?php echo form_close(); ?>
                
          </div>
        </div><!-- .hero-unit -->
    </div>
    <div class="col-md-2"></div>
</div>
<footer class="row">
    <p><small>Created by bunch of losers.</small></p>
</footer>

</div><!-- .container -->
    
</body>
   
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">window.jQuery || document.write('<script src="jquery/jquery-1.11.0.min.js"><\/script>');</script>
<script type="text/javascript" src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">'use strict';</script>

</html>