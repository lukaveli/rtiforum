<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>RTI Forum</title>
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css">
	<style type="text/css">
		body {
			padding: 70px;
      padding-bottom: 0;
		}
	</style>

</head>

<body>

<div class="container">
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">   

      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="../main.php">RTI Forum</a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <div class="col-md-4">
          <ul class="nav navbar-nav">
            <li><a href="../main.php">Kuća</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Godine <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="./prva/prva.php">Prva</a></li>
                <li><a href="#">Druga</a></li>
                <li><a href="#">Treća</a></li>
                <li><a href="#">Ćetvrta</a></li>
                <li class="divider"></li>
                <li><a href="#">Master</a></li>
                <li class="divider"></li>
                <li><a href="#">Doktorske</a></li>
              </ul>
            </li>
            <li><a href="../oglasi.php">Oglasi</a></li>
          </ul>
        </div><!--Linkovi-->

        <div class="col-md-6">
          <div class="pull-right">
            <ul class="nav navbar-nav">
              <li><a><span><span class="glyphicon glyphicon-search"></span></a></li>
              <li><a><span><span class="glyphicon glyphicon-comment"></span></a></li>
              <li><a><span><span class="glyphicon glyphicon-bell"></span></a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  slika
                  <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="../profile.php">Moj profil</a></li>
                  <li><a href="#">Podesavanja</a></li>
                  <li class="divider"></li>
                  <li><a href="../index.php">Logout</a></li>
                </ul>
              </li>
            </ul><!--profil notifikacije-->

          </div>
        </div>
    </div>
  </nav><!--navbar-->

  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-7">
       <ul class="breadcrumb">
        <li><a href="./main.php">RTI Forum</a></li>
        <li><a href="./main.php">Kuca</a></li>
        <li class="active">Prva godina</li>
      </ul>
    </div>
    <div class="col-md-3">
      <center>
        <button type="submit" class="btn btn-danger btn-block">Prati predmet</button>
      </center>
    </div>

    <div class="col-md-1"></div>
  </div>

  <div class="row">
    <div class="col-md-1">
    </div>

    <!--GLAVNI DEO-->

    <div class="col-md-7">
      <!--LISTA PREDMETA-->

      
    </div>

    <div class="col-md-1">
    </div>

  </div><!--row 1-->

  <footer class="row">
     <p><small>Created by bunch of losers.</small></p>
  </footer>
</body>  
   

</div><!-- .container -->

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">window.jQuery || document.write('<script src="../jquery/jquery-1.11.0.min.js"><\/script>');</script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">'use strict';</script>
<script type="text/javascript" src="../jquery/functions.js"></script>

</html>
