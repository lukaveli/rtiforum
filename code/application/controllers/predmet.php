<?php

class Predmet extends CI_Controller {
    
     function __construct() {
        parent::__construct();
        $this->load->model('predmet_model', 'predmet');
        $this->load->model('search_model', 'search');
    }
    
    public function index() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        
        $predmeti = $this->predmet->get();
        $search = $this->search->get();
        
        $data = array();
        $data['javascript'] = array('predmet', 'pretraga', 'najnoviji', 'notifikacije');
        $data['slika'] = $sess_data['slika'];
        $data['kuca_active'] = '';
        $data['predmeti_active'] = 'active';
        $data['oglasi_active'] = '';
        $data['predmeti'] = $predmeti;
        $data['search'] = $search;
        
        $korisnik_id = $sess_data['id'];
        $query = $this->db->get_where('slusa', array('korisnik_id' => $korisnik_id));
        
        $subscribed = array();
        foreach($query->result() as $row) {
            $subscribed[$row->predmet_id] = $predmeti[$row->predmet_id];
        }
        
        $data['subscribed'] = $subscribed;
        $data['korisnik_admin'] = $sess_data['admin'];
        $data['panel_active']='';
        $this->load_views($data);
    }
    
    private function load_views($data) {
        $this->load->view('header_view');
        $this->load->view('navbar_view', $data);
        $this->load->view('pages/predmeti_page', $data);
        $this->load->view('footer_view', $data);
    }

    public function subscribe() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        
        $korisnik_id = $sess_data['id'];
        $predmet_id = $this->input->post('predmet_id');
        
        $this->load->model('slusa');
        $this->slusa->populate(array('korisnik_id' => $korisnik_id, 'predmet_id' => $predmet_id));
        $this->slusa->save();
        
        $this->predmet->load($predmet_id);
        $data = array('predmet' => $this->predmet, 'to_subscribe' => FALSE);
        $this->load->view('predmet_view', $data);
    }
    
    public function unsubscribe() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        
        $korisnik_id = $sess_data['id'];
        $predmet_id = $this->input->post('predmet_id');
        
        $this->db->delete('slusa', array('korisnik_id' => $korisnik_id, 'predmet_id' => $predmet_id));
        
        $this->predmet->load($predmet_id);
        $data = array('predmet' => $this->predmet, 'to_subscribe' => TRUE);
        $this->load->view('predmet_view', $data);
    }
}