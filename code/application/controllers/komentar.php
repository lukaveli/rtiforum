<?php

function idrsortkomentar($a, $b) {
    return $b->komentar_id - $a->komentar_id;
}

class Komentar extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('komentar_model', 'komentar');
    }
    
    public function load_for_status() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        $status_id = $this->input->post('status_id');
        
        $query = $this->db->get_where('komentar', array('status_id' => $status_id));
        $rows = $query->result();
        usort($rows, 'idrsortkomentar');
        $this->load->model('korisnik');
        foreach($rows as $row) {
            $this->korisnik->load($row->korisnik_id);
            
            $komentar = array(
                'komentar_id' => $row->komentar_id,
                'status_id' => $status_id,
                'komentar_telo' => $row->komentar_telo,
                'komentar_datum' => $row->komentar_datum,
            );
            $s_data = array(
                'id' => $this->korisnik->korisnik_id,
                'ime' => $this->korisnik->korisnik_ime,
                'prezime' => $this->korisnik->korisnik_prezime,
                'slika' => $this->korisnik->korisnik_slika,
                'korisnik_admin' => $sess_data['admin']
            );
            
            $data = $this->makedata($komentar, $s_data);
            $this->load->view('komentar_view', $data);
        }
    }
    
    public function nadjeno(){
        $komentar_query = $this->db->get_where('komentar', array('status_id' => $this->input->post('status_id')));
        $komentar_models = $komentar_query->result();
        foreach($komentar_models as $komentar_model){
            $this->load->model('korisnik');
            $this->korisnik->load($komentar_model->korisnik_id);
            $data = array(
                'slika' => $this->korisnik->korisnik_slika,
                'ime' => $this->korisnik->korisnik_ime,
                'prezime' => $this->korisnik->korisnik_prezime,
                'datum' => $komentar_model->komentar_datum,
                'telo' => $komentar_model->komentar_telo
            );
            $this->load->view('komentar_view',$data);
        }
    }
    
    public function add() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('telo', 'Telo', 'required|xss_clean');
        
        //mozda nije ni potrebna validacija
        if($this->form_validation->run() == FALSE) {
            
        } else {
            $telo = $this->input->post('telo');
            $status_id = $this->input->post('status_id');
            $data = $this->store($telo, $status_id, $sess_data);
            $this->load->view('komentar_view', $data); 
        }
    }
    
    private function store($telo, $status_id, $sess_data) {
        $komentar = array(
                'komentar_id' => 0,
                'status_id' => $status_id,
                'komentar_telo' => $telo,
                'komentar_datum' => date('H:i - j.n.Y.'),
            );
        
        $data = $this->makedata($komentar, $sess_data);
        
        $row = array(
            'korisnik_id' => $data['korisnik_id'],
            'status_id' => $status_id,
            'komentar_datum' => $data['datum'],
            'komentar_telo' => $data['telo'],
        );
        
        $this->komentar->populate($row);
        $this->komentar->save();
        
        $data['komentar_id'] = $this->komentar->komentar_id;
                
        return $data;
    }
    
    private function makedata($komentar, $sess_data) {
        $data = array();
        
        $data['korisnik_id'] = $sess_data['id'];
        $data['status_id'] = $komentar['status_id'];
        $data['ime'] = $sess_data['ime'];
        $data['prezime'] = $sess_data['prezime'];
        $data['slika'] = $sess_data['slika'];
        $data['datum'] = $komentar['komentar_datum'];
        $data['telo'] = $komentar['komentar_telo'];
        $data['korisnik_admin'] = $sess_data['korisnik_admin'];
                
        return $data;
    } 
}