<?php

class Profil extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('korisnik');
        $this->load->model('slusa');
        $this->load->model('predmet_model');
        $this->load->model('komentar_model');
    }
    
     public function index() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        
        $data = $this->napravipodatke(0);
        $data['korisnik_admin'] = $sess_data['admin'];
        $data['panel_active']='';
        $this->load_views($data);
        
        
    }
    
    public function profilishkec(){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        if($this->input->post('korisnik_id') == $sess_data['id'])
            echo "<script type='text/javascript'>window.location.replace(\"profil\");;</script>";
        else{
            $data = $this->napravipodatke($this->input->post('korisnik_id'));
            $this->load->view('pages/profil_other', $data);
        }
    }
    
    
    public function izmeni(){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        $this->load->model('korisnik');
        $this->korisnik->load($sess_data['id']);
        $row = array();
        if($this->input->post('ime_korisnika')){
            $row['korisnik_ime'] = $this->input->post('ime_korisnika');
        } else {
            $row['korisnik_ime'] = $this->korisnik->korisnik_ime;
        }
        if($this->input->post('prezime_korisnika')){
            $row['korisnik_prezime'] = $this->input->post('prezime_korisnika');
        } else {
            $row['korisnik_prezime'] = $this->korisnik->korisnik_prezime;
        }
        $row['korisnik_hobbies'] = $this->input->post('hobiji_korisnika');
        
        $config = array(
            'upload_path' => 'images',
            'allowed_types' => 'jpg|jpeg|png',
            'max_size' => 250,
            'max_width' => 1920,
            'max_height' => 1080
        );
        
        
        $file_path = 'no_file';
        $this->load->library('upload', $config);
        if($this->upload->do_upload() == FALSE) {
            
        } else {
            $file_data = $this->upload->data();
            $this->korisnik->korisnik_slika= "images/".$file_data['file_name'];
            $row['korisnik_slika'] = $this->korisnik->korisnik_slika;
            $sess_data['slika'] = $row['korisnik_slika'];
        }
        $this->korisnik->populate($row);
        $this->korisnik->save();
        
        $sess_data['ime'] = $row['korisnik_ime'];
        $sess_data['prezime'] = $row['korisnik_prezime'];
        $this->session->set_userdata('logged_in',$sess_data);
        
        $data = $this->napravipodatke(0);
        $this->load->view('pages/profil_page', $data);
    }
    
    private function napravipodatke($id){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        if($id==0){
            $this->korisnik->load($sess_data['id']);
        } else {
            $this->korisnik->load($id);
        }
        $data = array();
        
        $data['javascript'] = array('dropzone', 'profil', 'pretraga', 'najnoviji','ajaxfileupload', 'upload');
        
        $data['slika'] = $this->korisnik->korisnik_slika;
        $data['profilna_slika'] = $this->korisnik->korisnik_slika;
        $data['kuca_active'] = '';
        $data['predmeti_active'] = '';
        $data['oglasi_active'] = '';
        $data['korisnik_id'] = $this->korisnik->korisnik_id;
        $data['ime'] = $this->korisnik->korisnik_ime;
        $data['prezime'] = $this->korisnik->korisnik_prezime;
        $data['korisnik_email'] = $this->korisnik->korisnik_email;
        $data['korisnik_hobbies'] = $this->korisnik->korisnik_hobbies;
        $data['korisnik_joined'] = $this->korisnik->korisnik_joined;
        $data['komentari'] = array();
        $data['komentari'] = $this->komentari($id);
        $data['pretplate'] = array();
        $data['pretplate'] = $this->pretplate($id);
        return $data;
    }
    
    public function brisi(){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        $data = $this->napravipodatke(0);
        $this->load->view('pages/profil_page', $data);
    }
    
    private function load_views($data) {
        $this->load->view('header_view');
        $this->load->view('navbar_view', $data);
        $this->load->view('pages/profil_page', $data);
        $this->load->view('footer_view', $data);
    }
       
    
    public function loadall() {
                $this->load->view('forma_o_korisniku');
                $sess_data = $this->session->userdata('logged_in');
                if($sess_data == FALSE) {
                    return;
                }
                $predmeti = $this->predmet->get();   
                $this->load->model('korisnik');
                foreach($predmeti as $p) {
                    $this->korisnik->load($p->korisnik_id);
                    if($s->status_tip == 1) {
                        $status = array(
                            'status_id' => $s->status_id,
                            'status_telo' => $s->status_telo,
                            'status_naslov' => $s->status_naslov,
                            'status_datum' => $s->status_datum,
                            'status_br_lajkova' => $s->status_br_lajkova,
                            'status_br_dislajkova' => $s->status_br_dislajkova,
                            'status_hidden' => $s->status_hidden,
                            'status_tip' => $s->status_tip,
                        );
                        $s_data = array(
                            'id' => $this->korisnik->korisnik_id,
                            'ime' => $this->korisnik->korisnik_ime,
                            'prezime' => $this->korisnik->korisnik_prezime,
                            'slika' => $this->korisnik->korisnik_slika,
                        );
                        $data = $this->makedata($status, $s_data);
                        $this->load->view('oglas_view', $data);
                    }
                }   
            
        
    }
    
    private function makedata($sess_data) {
        $data = array();
        
        $data['korisnik_id'] = $sess_data['id'];
        $data['ime'] = $sess_data['ime'];
        $data['prezime'] = $sess_data['prezime'];
        $data['slika'] = $sess_data['slika'];
        $data['korisnik_email'] = $sess_data['korisnik_email'];
        $data['korisnik_hobbies'] = $sess_data['korisnik_hobbies'];
        
        return $data;
    }
    
    public function pretplate($id){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        $finaldata = array();
        $temp = array();
        if($id==0){
            $temp['korisnik_id'] = $sess_data['id'];
        } else {
            $temp['korisnik_id'] = $id;
        }
        $slusa_query = $this->db->get_where('slusa', $temp);
        $slusa_models = $slusa_query->result();
        foreach($slusa_models as $slusa_model) {
            $query = $this->db->get_where('predmet', array('predmet_id' => $slusa_model->predmet_id));
            foreach($query->result() as $row) {
                $data = array();
                $model = new Predmet_Model();
                $model->populate($row);
                $data['predmet_skracenica'] = $row->predmet_skracenica;
                $data['predmet_opis'] = $row->predmet_opis;
                $finaldata[] = $data;
            }
        }
        return $finaldata;
    }
    
    public function komentari($id){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        $finaldata = array();
        $komentari = $this->komentar_model->get();
         if($id==0){
            $this->korisnik->load($sess_data['id']);
        } else {
            $this->korisnik->load($id);
        }
        foreach($komentari as $k) {
                    if($k->korisnik_id == $this->korisnik->korisnik_id) {
                        $data = array();
                        $data['komentar_datum'] = $k->komentar_datum;
                        $data['komentar_telo'] = $k->komentar_telo;
                        $finaldata[] = $data;
                    }
                }
        return $finaldata;
    }
}