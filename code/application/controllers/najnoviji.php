<?php

function idrsort($a, $b) {
    return $b->status_id - $a->status_id;
}

class Najnoviji extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('status_model', 'status');
    }
 
    public function load() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        
        $najnoviji_tip = $this->input->post('najnoviji_tip');
        switch($najnoviji_tip) {
            case 'oglas':
                $naslov = 'Najnoviji oglasi';
                $items = $this->najnoviji_oglasi();
                break;
            case 'file':
                $naslov = 'Najnoviji fajlovi';
                $items = $this->najnoviji_fajlovi($sess_data['id']);
                break;
        }
        
        usort($items, 'idrsort');
        
        $it = array();
        switch($najnoviji_tip) {
            case 'oglas':
                foreach ($items as $item) {
                    $it[] = $item->status_naslov;
                }
                break;
            case 'file':
                foreach ($items as $item) {
                    $it[] = $item->status_attachment;
                }
                break;
        }
        
        $data = array('items' => $it, 'naslov' => $naslov);
        
        $this->load->view('najnoviji_view', $data);
    }
    
    private function najnoviji_oglasi() {
        $items = array();
        
        $this->db->where(array('status_tip' => 2));
        $this->db->or_where(array('status_tip' => 3));
        $this->db->or_where(array('status_tip' => 4));
        $this->db->or_where(array('status_tip' => 5));
        $this->db->or_where(array('status_tip' => 6));
        $query = $this->db->get('status');

        foreach($query->result() as $row) {
            $oglas = new Status_Model();
            $oglas->populate($row);
            $items[] = $oglas;
        }
        
        return $items;
    }
    
    private function najnoviji_fajlovi($korisnik_id) {
        $items = array();
        
        $predmeti = $this->db->get_where('slusa', array('korisnik_id' => $korisnik_id));

        $this->db->where('status_tip', 1);
        if($predmeti->num_rows > 0) {
            $i = 0;
            foreach($predmeti->result() as $row) {
                if($i == 0) {
                    $this->db->where('predmet_id', $row->predmet_id  );
                } else {
                    $this->db->or_where('predmet_id', $row->predmet_id  );
                }
                $i++;
            }
            $query = $this->db->get('status');
        }
        
        if(isset($query)) {
            foreach($query->result() as $row) {
                $status = new Status_Model();
                $status->populate($row);
                if($status->status_attachment != 'no_file' && $status->status_attachment != NULL) {
                    $items[] = $status;
                }
            }
        }
        
        return $items;
    }
}