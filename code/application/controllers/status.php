<?php

/**
 * funkcija za sortiranje statusa po id-ju, u obrnutom redosledu
 */
function idrsort($a, $b) {
    return $b->status_id - $a->status_id;
}

class Status extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('status_model', 'status');
    }
    
    public function obrisistatus(){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        $status_id = $this->input->post('status_id');
        $this->db->delete('status', array('status_id' => $status_id));
        $this->db->delete('komentar', array('status_id' => $status_id));
        
    }
    
    public function loadall() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        
        $pocetni_id = $this->input->post('pocetni_status_id');
        
        
        $slusa_query = $this->db->get_where('slusa', array('korisnik_id' => $sess_data['id']));
        $slusa_models = $slusa_query->result();
        $statusi = array();
        foreach($slusa_models as $slusa_model) {
            if($pocetni_id != 0) {
                $this->db->where(array('predmet_id' => $slusa_model->predmet_id));
                $this->db->where(array('status_id <' => $pocetni_id));
                $query = $this->db->get('status');
            } else {
                $query = $this->db->get_where('status', array('predmet_id' => $slusa_model->predmet_id));
            }
            foreach($query->result() as $row) {
                $model = new Status_Model();
                $model->populate($row);
                $statusi[] = $model;
            }
        }
                
        usort($statusi, 'idrsort');    
        $this->load->model('korisnik');
        $num_items = count($statusi);
        $i = 0;
        foreach($statusi as $s) {
            $this->korisnik->load($s->korisnik_id);
            if($s->status_tip == 1) {
                $q = $this->db->get_where('predmet', array('predmet_id' => $s->predmet_id), 1, 0);
                $predmeti = $q->result();
                //ovaj foreach ce da izlista samo jedan ali mora ovako
                foreach($predmeti as $predmet) {
                    $status = array(
                        'status_id' => $s->status_id,
                        'predmet_skracenica' => $predmet->predmet_skracenica,
                        'status_telo' => $s->status_telo,
                        'status_datum' => $s->status_datum,
                        'status_br_lajkova' => $s->status_br_lajkova,
                        'status_br_dislajkova' => $s->status_br_dislajkova,
                        'status_hidden' => $s->status_hidden,
                        'status_tip' => $s->status_tip,
                        'status_attachment' => $s->status_attachment,
                    );
                }
                $s_data = array(
                    'id' => $this->korisnik->korisnik_id,
                    'ime' => $this->korisnik->korisnik_ime,
                    'prezime' => $this->korisnik->korisnik_prezime,
                    'slika' => $this->korisnik->korisnik_slika,
                    'korisnik_admin' => $sess_data['admin']
                );
                $data = $this->makedata($status, $s_data);
                
                if(($i === $num_items -1) || ($i === 8)) {
                    $data['last'] = TRUE;
                }
                
                $this->load->view('status_view', $data);
                
                $i++;
                if($i === 9) {
                    break;
                }
            }
        }   
    }
    
    public function add() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('telo', 'Telo', 'required|xss_clean');
        
        $this->form_validation->set_rules('predmet_id', 'Predmet', 'required');
        //mozda nije ni potrebna validacija
        if($this->form_validation->run() == FALSE) {
            $this->load->view('error_view');
        } else {
            $telo = $this->input->post('telo');
            $predmet_id = $this->input->post('predmet_id');
            $data = $this->store($telo, $predmet_id, $sess_data);  
            $this->load->view('status_view', $data);
        }
    }
    
    public function download() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        //$this->load->helper('download');
        
        $status_id = $this->input->post('status_id');
        $this->status->load($status_id);
        $name = $this->status->status_attachment;
        $file_path = base_url() . 'uploads/' . $name;
        
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        
        if($ext == 'zip' || $ext === 'rar') {
            echo 'status/download_url/'.$name;
        } else {
            echo $file_path;
        }
    }
    
    public function download_url($name) {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        $this->load->helper('download');
        $file_path = base_url() . 'uploads/' . $name;
        $data = file_get_contents($file_path);

        force_download($name, $data);
    }
    
    public function nadjeno(){
        $status_query = $this->db->get_where('status', array('status_id' => $this->input->post('status_id')));
        $status_models = $status_query->result();
        foreach($status_models as $status_model){
            $this->load->model('korisnik');
            $this->korisnik->load($status_model->korisnik_id);
            $this->load->model('predmet_model');
            $this->predmet_model->load($status_model->predmet_id);
            $data = array(
                'status_id' => $status_model->status_id,
                'slika' => $this->korisnik->korisnik_slika,
                'ime' => $this->korisnik->korisnik_ime,
                'prezime' => $this->korisnik->korisnik_prezime,
                'datum' => $status_model->status_datum,
                'telo' => $status_model->status_telo,
                'skracenica' => $this->predmet_model->predmet_skracenica
            );
            $this->load->view('searchStatus_view',$data);
        }      
    }
        
    private function store($telo, $predmet_id, $sess_data) {
        $config = array(
            'upload_path' => 'uploads',
            'allowed_types' => 'jpg|jpeg|png|pdf|doc|docx|zip|txt'
        );
        $file_path = 'no_file';
        $this->load->library('upload', $config);
        if($this->upload->do_upload() == FALSE) {
            
        } else {
            $file_data = $this->upload->data();
            $file_path = $file_data['file_name'];
        }
        $q = $this->db->get_where('predmet', array('predmet_id' => $predmet_id), 1, 0);
        $predmeti = $q->result();
        //ovaj foreach ce da izlista samo jedan ali mora ovako
        foreach($predmeti as $predmet) {
            $status = array(
                'status_id' => 0,
                'status_telo' => $telo,
                'status_datum' => date('H:i - j.n.Y.'),
                'status_br_lajkova' => 0,
                'status_br_dislajkova' => 0,
                'status_hidden' => 0,
                'status_tip' => 1,
                'predmet_skracenica' => $predmet->predmet_skracenica,
                'status_attachment' => $file_path,
            );
        }
        $data = $this->makedata($status, $sess_data);
        
        $row = array(
            'korisnik_id' => $data['korisnik_id'],
            'predmet_id' => $predmet_id,
            'status_datum' => $data['datum'],
            'status_telo' => $data['telo'],
            'status_br_lajkova' => $data['br_lajkova'],
            'status_br_dislajkova' => $data['br_dislajkova'],
            'status_tip' => $data['tip'],
            'status_hidden' => $data['hidden'],
            'status_attachment' => $data['attachment'],
        );
        
        $this->status->populate($row);
        $this->status->save();
        
        $data['status_id'] = $this->status->status_id;
                
        return $data;
    }
    
    private function makedata($status, $sess_data) {
        $data = array();
        
        $data['korisnik_id'] = $sess_data['id'];
        $data['status_id'] = $status['status_id'];
        $data['ime'] = $sess_data['ime'];
        $data['prezime'] = $sess_data['prezime'];
        $data['slika'] = $sess_data['slika'];
        $data['datum'] = $status['status_datum'];
        $data['telo'] = $status['status_telo'];
        $data['br_lajkova'] = $status['status_br_lajkova'];
        $data['br_dislajkova'] = $status['status_br_dislajkova'];
        $data['hidden'] = $status['status_hidden'];
        $data['tip'] = $status['status_tip'];
        $data['skracenica'] = $status['predmet_skracenica'];
        $data['attachment'] = $status['status_attachment'];
        $data['last'] = FALSE;
        $data['korisnik_admin'] = $sess_data['korisnik_admin']; 
        
        return $data;
    } 
    
    public function lajk() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        $this->load->model('lajkovao_model', 'lajkovao');
         
        $korisnik_id = $sess_data['id'];
        $status_id = $this->input->post('status_id');
        
        $query = $this->db->get_where('lajkovao',array('status_id' => $status_id, 'korisnik_id' => $korisnik_id),1,0);
        $status = new Status_Model();
        $status->load($status_id);
        
        if($query->num_rows() == 0) {
            $status->status_br_lajkova += 1;
            $status->save();
            
            $lajkovao = new Lajkovao_Model();
            $row = array('korisnik_id' => $korisnik_id, 'status_id' => $status_id);
            $lajkovao->populate($row);
            $lajkovao->save();
        }
               
        $data = array('br_lajkova' => $status->status_br_lajkova);    
        $this->load->view('lajk_view', $data);
    }
    
    public function dislajk() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        $this->load->model('dislajkovao_model', 'dislajkovao');
        
        $korisnik_id = $sess_data['id'];
        $status_id = $this->input->post('status_id');
        
        $query = $this->db->get_where('dislajkovao',array('status_id' => $status_id, 'korisnik_id' => $korisnik_id),1,0);
        $status = new Status_Model();
        $status->load($status_id);
 
        if($query->num_rows() == 0) {
            $status->status_br_dislajkova += 1;
            $status->save();
            
            $dislajkovao = new Dislajkovao_Model();
            $row = array('korisnik_id' => $korisnik_id, 'status_id' => $status_id);
            $dislajkovao->populate($row);
            $dislajkovao->save();
        }
        
        //loaduje view lajk koji je samo ispis br_lajkova, ili dislajkova, sta god se prosledi
        $data = array('br_lajkova' => $status->status_br_dislajkova);
        $this->load->view('lajk_view', $data);
    }
}
