<?php

class Search extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('search_model', 'tutorial_db');
    }
    
    public function index() {
//       
//        $html = '';
//        $html .= '<li class="result">';
//        $html .= '<a target="_blank" href="urlString">';
//        $html .= '<h3>nameString</h3>';
//        $html .= '<h4>functionString</h4>';
//        $html .= '</a>';
//        $html .= '</li>';
//
//        // Get Search
//        $search_string = preg_replace("/[^A-Za-z0-9]/", " ", $_POST['query']);
//        $search_string = $tutorial_db->real_escape_string($search_string);
//
//        // Check Length More Than One Character
//        if (strlen($search_string) >= 1 && $search_string !== ' ') {
//            // Build Query
//            $query = 'SELECT * FROM search WHERE function LIKE "%'.$search_string.'%" OR name LIKE "%'.$search_string.'%"';
//
//            // Do Search
//            $result = $tutorial_db->query($query);
//            while($results = $result->fetch_array()) {
//                    $result_array[] = $results;
//            }
//
//            // Check If We Have Results
//            if (isset($result_array)) {
//                foreach ($result_array as $result) {
//
//                    // Format Output Strings And Hightlight Matches
//                    $display_function = preg_replace("/".$search_string."/i", "<b class='highlight'>".$search_string."</b>", $result['string_function']);
//                    $display_name = preg_replace("/".$search_string."/i", "<b class='highlight'>".$search_string."</b>", $result['string_name']);
//                    $display_url = 'http://php.net/manual-lookup.php?pattern='.urlencode($result['string_function']).'&lang=en';
//
//                    // Insert Name
//                    $output = str_replace('nameString', $display_name, $html);
//
//                    // Insert Function
//                    $output = str_replace('functionString', $display_function, $output);
//
//                    // Insert URL
//                    $output = str_replace('urlString', $display_url, $output);
//
//                    // Output
//                    echo($output);
//                }
//            }else{
//
//                    // Format No Results Output
//                    $output = str_replace('urlString', 'javascript:void(0);', $html);
//                    $output = str_replace('nameString', '<b>No Results Found.</b>', $output);
//                    $output = str_replace('functionString', 'Sorry :(', $output);
//
//                    // Output
//                    echo($output);
//            }
//        }
//        
    }
    
    public function findall(){

        $query = $this->db->get('status');
        
        $query_mod = $query->result();
        
        
        foreach($query_mod as $q){
            $result = stripos($q->status_telo,$this->input->post('parametar'));
            if($result !== FALSE){
                $tip = "";
                if($q->status_tip == 1){
                    $tip .= "Status";                        
                } else { 
                    $tip .= "Oglas";
                    
                }
                $this->load->view('search_view',array('nasao' => $tip, 'opis' => $q->status_telo, 'status_id' => $q->status_id, 'status_tip' => $q->status_tip));

            }
        }
    }
        
}