<?php

class Proveravac extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('status_model', 'status');
    }
    
    public function proveri() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        
        $br_notifikacija = 0;
        $notifikacije = array();
        
        $korisnik_id = $sess_data['id'];
        $this->load->model('korisnik');
        $this->korisnik->load($korisnik_id);
        
        $predmeti = $this->db->get_where('slusa', array('korisnik_id' => $korisnik_id));
        
        $trenutni_status_id = $sess_data['najnoviji_status_id'];
        
        $noviji_statusi_niz = array();
        
        if($predmeti->num_rows > 0) {
            foreach($predmeti->result() as $row) {
                $this->db->where('status_id >', $trenutni_status_id);
                $this->db->where('status_tip', 1);
                $this->db->where('predmet_id', $row->predmet_id);
                $noviji_statusi_niz[] = $this->db->get('status');
            }
        }
        $status_id_niz = array();
        
        foreach($noviji_statusi_niz as $noviji_statusi) {
            if($noviji_statusi->num_rows() > 0) {
                foreach($noviji_statusi->result() as $status) {
                    if($status->korisnik_id !== $korisnik_id) {
                        $notifikacije[$br_notifikacija] = $this->napravi_status_notifikaciju($status);
                        $status_id_niz[$br_notifikacija] = $status->status_id;
                        $br_notifikacija++;
                    }
                }
            }
        }
        
                
        $data = array(
            'br_notifikacija' => $br_notifikacija,
            'notifikacije' => $notifikacije,
            'status_id_niz' => $status_id_niz,
        );
        
        $this->load->view('notifikacije_view', $data);
   }
    
    private function napravi_status_notifikaciju($status) {
        $notifikacija = array();
        
        $this->korisnik->load($status->korisnik_id);
        $notifikacija['ime'] = $this->korisnik->korisnik_ime;
        $notifikacija['telo'] = 'je dodao novi status na predmet: ';
        $this->load->model('predmet_model', 'predmet');
        $this->predmet->load($status->predmet_id);
        $notifikacija['skracenica'] = $this->predmet->predmet_skracenica;
        
        return $notifikacija;
    }
    
    public function najnoviji() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        
        $najnoviji_id = $this->input->post('status_id');
        $trenutni_id = $sess_data['najnoviji_status_id'];
        if($najnoviji_id > $trenutni_id) {
            $sess_data['najnoviji_status_id'] = $najnoviji_id;
            $this->session->set_userdata('logged_in', $sess_data);
            $this->load->view('lajk_view', array('br_lajkova' => $najnoviji_id));//IZBRISI OVO
        }
    }
    
}