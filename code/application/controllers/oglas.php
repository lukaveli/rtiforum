<?php

/**
 * funkcija za sortiranje statusa po id-ju, u obrnutom redosledu
 */
function idrsort($a, $b) {
    return $b->status_id - $a->status_id;
}

class Oglas extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('status_model', 'status');
    }
   
    public function index() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        $data = array();
        $data['javascript'] = array('oglas', 'najnoviji', 'pretraga', 'notifikacije');
        $data['slika'] = $sess_data['slika'];
        $data['kuca_active'] = '';
        $data['predmeti_active'] = '';
        $data['oglasi_active'] = 'active';
        $data['korisnik_admin'] = $sess_data['admin'];
        $data['panel_active']='';
        $this->load_views($data);
    }
    
    public function obrisioglas(){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        $status_id = $this->input->post('status_id');
        $this->db->delete('status', array('status_id' => $status_id));
    }
    
    private function load_views($data) {
        $this->load->view('header_view');
        $this->load->view('navbar_view', $data);
        $this->load->view('pages/oglasi_page');
        $this->load->view('footer_view', $data);
    }

    public function loadall() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        
        $tip = $this->input->post('tip');
        
        $oglas_query = $this->db->get_where('status', array('status_tip' => $tip));
        $oglasi = $oglas_query->result();
        usort($oglasi, 'idrsort');

        $this->load->model('korisnik');
        foreach($oglasi as $s) {
            $this->korisnik->load($s->korisnik_id);
            $oglas = array(
                'status_id' => $s->status_id,
                'status_telo' => $s->status_telo,
                'status_naslov' => $s->status_naslov,
                'status_datum' => $s->status_datum,
                'status_br_lajkova' => $s->status_br_lajkova,
                'status_br_dislajkova' => $s->status_br_dislajkova,
                'status_hidden' => $s->status_hidden,
                'status_tip' => $s->status_tip,
            );
            $s_data = array(
                'id' => $this->korisnik->korisnik_id,
                'ime' => $this->korisnik->korisnik_ime,
                'prezime' => $this->korisnik->korisnik_prezime,
                'slika' => $this->korisnik->korisnik_slika,
                'korisnik_admin' => $sess_data['admin']
            );
            $data = $this->makedata($oglas, $s_data);
            $this->load->view('oglas_view', $data);
        }
    }
    
    public function add() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules(array(
            array(
                'field' => 'telo',
                'label' => 'Telo',
                'rules' => 'required|xss_clean'
            ),
            array(
                'field' => 'naslov',
                'label' => 'Naslov',
                'rules' => 'required|xss_clean'
            )
        ));
                
        if($this->form_validation->run() == FALSE) {
            
        } else {
            $telo = $this->input->post('telo');
            $naslov = $this->input->post('naslov');
            $tip = $this->input->post('tip');
            $data = $this->store($telo, $naslov, $tip, $sess_data);
            $this->load->view('oglas_view', $data); 
        }
    }
    
    private function store($telo, $naslov, $tip, $sess_data) {
        $status = array(
            'status_id' => 0,//privremeni id, ispod se menja
            'status_telo' => $telo,
            'status_naslov' => $naslov,
            'status_datum' => date('H:i - j.n.Y.'),
            'status_br_lajkova' => 0,
            'status_br_dislajkova' => 0,
            'status_hidden' => 0,
            'status_tip' => $tip,
        );
        $data = $this->makedata($status, $sess_data);
        
        $row = array(
            'status_datum' => $data['datum'],
            'status_telo' => $data['telo'],
            'status_naslov' => $data['naslov'],
            'korisnik_id' => $data['korisnik_id'],
            'status_tip' => $data['tip'],
            'status_hidden' => $data['hidden'],
        );
        
        $this->status->populate($row);
        $this->status->save();
        
        $data['status_id'] = $this->status->status_id;
        
        return $data;
    }
    
    private function makedata($status, $sess_data) {
        $data = array();
        
        $data['korisnik_id'] = $sess_data['id'];
        $data['status_id'] = $status['status_id'];
        $data['ime'] = $sess_data['ime'];
        $data['prezime'] = $sess_data['prezime'];
        $data['slika'] = $sess_data['slika'];
        $data['datum'] = $status['status_datum'];
        $data['telo'] = $status['status_telo'];
        $data['naslov'] = $status['status_naslov'];
        $data['br_lajkova'] = $status['status_br_lajkova'];
        $data['br_dislajkova'] = $status['status_br_dislajkova'];
        $data['hidden'] = $status['status_hidden'];
        $data['tip'] = $status['status_tip'];
        $data['korisnik_admin'] = $sess_data['korisnik_admin'];
        
        return $data;
    } 
    
   
    
}
