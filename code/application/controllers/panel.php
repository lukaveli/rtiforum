<?php

class Panel extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('korisnik');
    }
    
     public function index() {
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            redirect('welcome');
        }
        
       $data = array();
        $data['javascript'] = array('panel','pretraga', 'notifikacije');
        $data['slika'] = $sess_data['slika'];
        $data['kuca_active'] = '';
        $data['predmeti_active'] = '';
        $data['oglasi_active'] = '';
        $data['korisnik_admin'] = $sess_data['admin'];
        $data['panel_active']='active';
        $data['korisnici'] = array();
        $data['korisnici'] = $this->uzmikorisnike();
        $this->load_views($data);
  
    } 
    
    public function uzmikorisnike(){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        $finaldata = array();
        $korisnici = $this->korisnik->get();

        foreach($korisnici as $k) {              
                        $data = array();
                        $data['korisnik_email'] = $k->korisnik_email;
                        $data['korisnik_id'] = $k->korisnik_id;
                        $data['korisnik_admin'] = $k->korisnik_admin;
                        $finaldata[] = $data;
        }
        return $finaldata;
    }

    
    
    private function load_views($data) {
        $this->load->view('header_view');
        $this->load->view('navbar_view', $data);
        $this->load->view('pages/panel_page', $data);
        $this->load->view('footer_view', $data);
    }
       
    
    
    private function makedata($sess_data) {
        $data = array();
        
        $data['korisnik_id'] = $sess_data['id'];
        $data['ime'] = $sess_data['ime'];
        $data['prezime'] = $sess_data['prezime'];
        $data['slika'] = $sess_data['slika'];
        $data['korisnik_email'] = $sess_data['korisnik_email'];
        $data['korisnik_hobbies'] = $sess_data['korisnik_hobbies'];
        
        return $data;
    }
    
    public function obrisi(){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        $korisnik_id = $this->input->post('korisnik_id');
        $this->db->delete('korisnik', array('korisnik_id' => $korisnik_id));
    }
    
    public function promo(){
        $sess_data = $this->session->userdata('logged_in');
        if($sess_data == FALSE) {
            return;
        }
        $korisnik_id = $this->input->post('korisnik_id');
        $this->korisnik->load($korisnik_id);
        $this->korisnik->korisnik_admin = 1;
        $this->korisnik->save();
    }

}
