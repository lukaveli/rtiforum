<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('korisnik', '', TRUE);
    }
    
    function index() { 
        $this->load->helper(array('form'));
        $this->load_views();
    }
    
    private function load_views() {
        $this->load->view('header_view');
        $this->load->view('pages/welcome_page');
    }
    
    function verify_login() {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

        if($this->form_validation->run() == FALSE) {
            redirect('welcome');
        } else {
            redirect('home');
        }
    }
    
    function check_database($password) {
        $email = $this->input->post('email');
        
        $result = $this->login($email, $password);

        if($result) {
            $sess_array = array();
            foreach($result as $row) {
                $sess_array = array(
                    'id' => $row->korisnik_id,
                    'email' => $row->korisnik_email,
                    'ime' => $row->korisnik_ime,
                    'prezime' => $row->korisnik_prezime,
                    'slika' => $row->korisnik_slika,
                    'admin' => $row->korisnik_admin
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }
    
    function login($email, $password) {
        $this->db->select('korisnik_id, korisnik_email, korisnik_password,'
                . ' korisnik_ime, korisnik_prezime, korisnik_slika, korisnik_admin');
        $this->db->from('korisnik');
        $this->db->where('korisnik_email', $email);
        $this->db->where('korisnik_password', md5($password));
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    
    function verify_register() {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|'
                . 'is_unique[korisnik.korisnik_email]|min_length[6]|max_length[30]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        if($this->form_validation->run() == FALSE) {
            $this->load->view('pages/welcome_page');
        } else {
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            $sex = $this->input->post('sex');
            $this->register($email, $password, $sex);
        }
    }
    
    function register($email, $password, $sex) {
        $ime = "Aleksandar";
        $prezime = "Savic";
        $slika = "images/savic.jpg";
        switch($sex) {
            case 'Male':
                $ime = "John";
                $prezime = "Doe";
                $slika = "images/john_doe.jpg";
                break;
            case 'Female':
                $ime = "Jane";
                $prezime = "Doe";
                $slika = "images/jane_doe.jpg";
                break;
        }
        
        $row = array(
            'korisnik_email' => $email,
            'korisnik_password' => $password,
            'korisnik_ime' => $ime,
            'korisnik_prezime' => $prezime,
            'korisnik_tip' => 1,
            'korisnik_joined' => date('H:i - j.n.Y.'),
            'korisnik_slika' => $slika,
        );
        
        $this->korisnik->populate($row);
        $this->korisnik->save();
        
        $sess_array = array(
                    'id' => $this->korisnik->korisnik_id,
                    'email' => $this->korisnik->korisnik_email,
                    'ime' => $this->korisnik->korisnik_ime,
                    'prezime' => $this->korisnik->korisnik_prezime,
                    'slika' => $this->korisnik->korisnik_slika,
                    'admin' => $this->korisnik->korisnik_admin
                );
        $this->session->set_userdata('logged_in', $sess_array);
        
        $query = $this->db->get_where('predmet', array('predmet_skracenica' => 'Info'));
        $korisnik_id = $this->korisnik->korisnik_id;
        foreach($query->result() as $info) {
            $predmet_id = $info->predmet_id;
        }
        $this->load->model('slusa');
        $this->slusa->populate(array('korisnik_id' => $korisnik_id, 'predmet_id' => $predmet_id));
        $this->slusa->save();
        
        redirect('home');
    }
}