<?php

class Home extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $sess_data = $this->session->userdata('logged_in'); 
        if($sess_data) {
            $data = $this->makedata($sess_data);
            $this->load_views($data);
        } else {
            redirect('welcome');
        }
    }
    
    private function load_views($data) {
        $this->load->view('header_view');
        $this->load->view('navbar_view', $data);
        $this->load->view('pages/home_page', $data);
        $this->load->view('footer_view', $data);
    }
    
    private function makedata($sess_data) {
        $data = array();
        
        $korisnik_id = $sess_data['id'];  
        $data['predmeti'] = $this->dohvati_predmete($korisnik_id);
        
        $data['slika'] = $sess_data['slika'];
        
        $data['javascript'] = array(
            'ajaxfileupload', 'home', 'pretraga', 'najnoviji', 'notifikacije'
        );
          
        $data['kuca_active'] = 'active';
        $data['predmeti_active'] = '';
        $data['oglasi_active'] = '';
        $data['korisnik_admin'] = $sess_data['admin'];
        $data['panel_active']='';
        return $data;
    }
    
    //u data smesta listu predmeta koje tekuci korisnik slusa
    private function dohvati_predmete($korisnik_id) {
        $query = $this->db->get_where('slusa', array('korisnik_id' => $korisnik_id));
        
        $predmeti = array();
        if($query->num_rows() > 0) {
            $this->load->model('predmet_model', 'predmet');
            $models = $this->predmet->get();
            foreach($query->result() as $row) {
                $id = $row->predmet_id;
                $skracenica = "";
                foreach($models as $model) {
                    if($model->predmet_id === $id) {
                        $skracenica = $model->predmet_skracenica;
                        break;
                    }
                }
                $predmeti[] = array('id' => $id, 'skracenica' => $skracenica);
            }
        }
        
        return $predmeti;
    }
    
    public function logout() {
        if($this->session->userdata('logged_in')) {
            $this->session->sess_destroy();
            redirect('welcome');
        }
    }
    
}
